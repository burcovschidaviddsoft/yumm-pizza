<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorii extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('categorii_model', 'categoriim');
        $this->load->model('produse_model', 'produsem');
    	$this->load->model('ingrediente_model', 'ingredientem');
    }

	public function index( $slug_categorie = "" )
	{

		if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }
        if( !empty($this->session->flashdata('error')) ) {
            $data['error'] = $this->session->flashdata('error');
        }

		if( isset($slug_categorie) && !empty($slug_categorie) ) {

			$data['categorie'] = $this->categoriim->get_categorie_by_slug( $slug_categorie );
			if( isset($data['categorie']) && !empty($data['categorie']) ) {

				$produse = $this->produsem->get_produse_categorie($data['categorie']['id']);

				$k = 0;
				foreach ($produse as $produs) {
					$data['produse'][] = $produs;
					$data['produse'][$k]['optiuni'] = $this->produsem->get_optiuni_pe_clase($produs['id']);
					$data['produse'][$k]['ingrediente'] = $this->produsem->get_ingrediente($produs['id']);
					$k++;
				}
			} else {
				#get all products
				$produse = $this->produsem->get_produse();

				$k = 0;
				foreach ($produse as $produs) {
					$data['produse'][] = $produs;
					$data['produse'][$k]['optiuni'] = $this->produsem->get_optiuni_pe_clase($produs['id']);
					$data['produse'][$k]['ingrediente'] = $this->produsem->get_ingrediente($produs['id']);
					$k++;
				}
			}

		} else {
			#get all products
			$produse = $this->produsem->get_produse();

			$k = 0;
			foreach ($produse as $produs) {
				$data['produse'][] = $produs;
				$data['produse'][$k]['optiuni'] = $this->produsem->get_optiuni_pe_clase($produs['id']);
				$data['produse'][$k]['ingrediente'] = $this->produsem->get_ingrediente($produs['id']);
				$k++;
			}
		}

		$data["ingrediente"] = $this->ingredientem->get_ingrediente_by_categorie($data['categorie']['id']);
		$data["page_view"] = "categorii";
		//die(json_encode($data['produse']));
        $this->load->library('display', $data);
	}
}
