<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cos extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('produse_model', 'produsem');
    }

	public function index()
	{
		if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }
        if( !empty($this->session->flashdata('error')) ) {
            $data['error'] = $this->session->flashdata('error');
        }

        if( !empty($this->session->userdata('cos')) ) {

        	$data['produse'] = array();

        	$k = 0;
        	$data['total'] = 0;
        	foreach ($this->session->userdata('cos') as $produs) {

        		$data['produse'][] = $produs;
        		$data['produse'][$k]['image'] = $this->produsem->get_produs($produs['id_produs'])['image'];
        		$data['total'] += $produs['pret']*$produs['cantitate'];

                foreach ($produs['optiuni'] as $optiune) {
                    $data['produse'][$k]['op'][] = $this->produsem->get_optiune($optiune);
                }

        		$k++;
        	}

        	/*echo "<pre>";
            print_r($this->session->userdata('cos'));
        	print_r($data['produse']);
        	echo "</pre>";*/

        }

		$data["page_view"] = "cos";
        $this->load->library('display', $data);
	}

	public function adauga() {

		$this->load->helper('security');
        $this->load->library('form_validation');
        die(json_encode($this->input->post()['optiuni']));
        $this->form_validation->set_rules('id_produs', 'Produs', 'trim|required|numeric');
        $this->form_validation->set_rules('marime', 'Marime', 'trim|required|callback_marime');
        $this->form_validation->set_rules('optiuni', 'Optiuni', 'trim');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();

            $produs = $this->produsem->get_produs(xss_clean($p['id_produs']));
        	
        	if($p['marime'] == 1) {

        		$marime = $produs['mica'];
        		$pret = $produs['pret_mica'];

        	} elseif($p['marime'] == 2) {

				$marime = $produs['medie'];
        		$pret = $produs['pret_medie'];

        	} elseif($p['marime'] == 3) {

        		$marime = $produs['mare'];
        		$pret = $produs['pret_mare'];
        		
        	}

        	if (empty($p['optiuni'])) {
        		$p['optiuni'] = array();
        	}

            $prod = array('id_produs' => xss_clean($p['id_produs']), 'nume' => xss_clean($produs['nume']), 'marime' => xss_clean($marime), 'pret'=> xss_clean($pret), 'cantitate' => 1, 'optiuni' => $p['optiuni']);

            if( !empty($this->session->userdata('cos')) ) {

            	$cos = $this->session->userdata('cos');
            	#verificam daca produsul exista deja in cos
            	$ok = 0;
            	for($i=0; $i<count($cos); $i++) {
            		if($cos[$i]['id_produs'] == $prod['id_produs'] && $cos[$i]['marime'] == $prod['marime'] && $cos[$i]['pret'] == $prod['pret']) {
            			$ok = 1;
            			$cos[$i]['cantitate']++;
            		}
            	}
            	if($ok == 0) {
            		$cos[count($this->session->userdata('cos'))] = $prod;
            	}

            } else {
            	$cos = array(0 => $prod);
            }

        	$this->session->set_userdata('cos', $cos);

            $this->session->set_flashdata('success', "Produsul a fost adaugat in cos");
        }

        if( validation_errors()!="" ) {
        	$this->session->set_flashdata('error', validation_errors());
        }

        redirect('cos');
	}

	function marime( $marime ) {

		if($marime == 1 || $marime == 2 || $marime == 3) {
			return true;
		} else {
			$this->form_validation->set_message("marime", "Va rugam selectati o marime de pizza valida.");
            return false;
		}

	}

	public function sterge($i) {

		$cos = $this->session->userdata('cos');

		array_splice($cos,$i,1);

		$this->session->unset_userdata('cos');
        $this->session->set_userdata('cos', $cos);

		$this->session->set_flashdata('success', "Produsul a fost sters din cos");
		redirect('cos');

	}

	public function salveaza() {

		$this->load->helper('security');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('cantitate[]', 'Cantitate', 'trim|required|numeric');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();
			$cos = $this->session->userdata('cos');

			for($i=0;$i<count($cos);$i++) {

				if(xss_clean($p['cantitate'][$i]) > 0) {
					$cos[$i]['cantitate'] = xss_clean($p['cantitate'][$i]);
				} else {
					array_splice($cos,$i,1);
				}
			}

            $this->session->unset_userdata('cos');
        	$this->session->set_userdata('cos', $cos);
            $this->session->set_flashdata('success', "Cosul de cumparaturi a fost actualizat");

        }

		redirect('cos');
	}

	public function checkout() {

		if( !$this->simpleloginsecure->is_logat() ) {
            $this->session->set_flashdata('error', "Trebuie sa fii autentificat pentru a trimite comanda");
            redirect('login?link='.site_url('cos/checkout'));
        }
        echo "<pre>";
        print_r($this->session->userdata());
        echo "</pre>";

    	$data["page_view"] = "checkout";
        $this->load->library('display', $data);
	}

    public function trimite_comanda() {

        $this->load->helper('security');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('adr', 'Trimiteti la alta adresa', 'trim');
        $this->form_validation->set_rules('detalii', 'Alte detalii', 'trim');
        $this->form_validation->set_rules('plata', 'Plata', 'trim|required');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();
            $cos = $this->session->userdata('cos');

            if(!isset($p['adr']) || empty($p['adr'])) {
                $p['adr'] = $this->session->userdata('login')['adr'];
            }

            //inseram comanda
            $this->db->set('adr', xss_clean($p['adr']));
            $this->db->set('detalii', xss_clean($p['detalii']));
            $this->db->set('plata', xss_clean($p['plata']));
            $this->db->set('id_user', $this->session->userdata('login')['id']);

            if($this->db->insert('comenzi')) {

                $id_comanda = $this->db->insert_id();

                foreach ($cos as $produs) {
                    $this->db->set('id_comanda', $id_comanda);
                    $this->db->set('id_produs', $produs['id_produs']);
                    $this->db->set('marime', $produs['marime']);
                    $this->db->set('pret', $produs['pret']);
                    $this->db->set('cantitate', $produs['cantitate']);

                    $this->db->insert('produse_comanda');

                    foreach ($produs['optiuni'] as $optiune) {

                        $this->db->set('id_comanda', $id_comanda);
                        $this->db->set('id_produs', $produs['id_produs']);
                        $this->db->set('id_optiune', $optiune);
                        $this->db->insert('optiuni_produse_comanda');
                    }
                }

                $this->session->unset_userdata('cos');

                $data['success'] = "Comanda a fost trimisa";
            }

            $data["page_view"] = "comanda_trimisa";
        } else {
            $data["page_view"] = "checkout";

        }

        

        $this->load->library('display', $data);

    }
}
