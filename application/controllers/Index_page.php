<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_page extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('produse_model', 'produsem');
    }

	public function index()
	{
		$data['produse'] = array();
		$produse = $this->produsem->get_produse_evidentiate();
		$k = 0;
		foreach ($produse as $produs) {
			$data['produse'][] = $produs;
			$data['produse'][$k]['optiuni'] = $this->produsem->get_optiuni_produs($produs['id']);

			$k++;
		}

		$data["page_view"] = "index_page";
        $this->load->library('display', $data);
	}
}
