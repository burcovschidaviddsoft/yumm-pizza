<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
        parent::__construct();

		$this->load->helper('security');
        
    }

	public function index( $data = array() )
	{

		if( $this->session->flashdata('success')!="" ) {
            $data['success'] = $this->session->flashdata('success');
        }
        if( $this->session->flashdata('error')!="" ) {
            $data['errorLogin'] = $this->session->flashdata('error');
        }

        $data['link'] = xss_clean($this->input->get('link'));

		$data["page_view"] = "login";
        $this->load->library('display', $data);
	}

	public function verificare() {
        $data = array();

		if( $this->simpleloginsecure->login( $this->input->post('user_email_login'), $this->input->post('user_pass_login') ) ) {

			$data['link'] = xss_clean($this->input->get('link'));
			$link = urldecode( $data['link'] );

            if( $link!="" ) {
                redirect($link);
            } else {
				redirect();
            }

		} else {
            $data["errorLogin"] = "Adresa de email sau parola gresita.";
        }

        $this->index($data);

	}

	public function inregistrare() {

		$data['link'] = xss_clean($this->input->get('link'));

		$data["page_view"] = "inregistrare";
        $this->load->library('display', $data);

	}

	public function salveaza() {

		$data['link'] = xss_clean($this->input->get('link'));

		$this->load->helper('security');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nume', 'Nume', 'trim|required');
        $this->form_validation->set_rules('tel', 'Telefon', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('parola', 'Parola', 'trim|required');
        $this->form_validation->set_rules('oras', 'Oras', 'trim|required');
        $this->form_validation->set_rules('adr', 'Adresa', 'trim|required');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();

            $this->db->set('nume', xss_clean($p['nume']));
            $this->db->set('telefon', xss_clean($p['tel']));
            $this->db->set('user_email', xss_clean($p['email']));
            $this->db->set('user_pass', md5(xss_clean($p['parola'])));
            $this->db->set('tip', 2);
            $this->db->set('oras', xss_clean($p['oras']));
            $this->db->set('adr', xss_clean($p['adr']));

            if($this->db->insert('useri')) {

            	$this->session->set_flashdata('success', "Contul a fost creat");
            	if( $data['link'] != "" ) {
            		redirect($data['link']);
            	} else {
            		redirect('login');
            	}
            }
        }

        $data["page_view"] = "inregistrare";
        $this->load->library('display', $data);

	}

	function logout() {
        $this->simpleloginsecure->logout();
        redirect();
    }
}
