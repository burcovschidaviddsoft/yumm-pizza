<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorii extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if( !$this->simpleloginsecure->is_logat() ) {
            redirect( 'admin/login/' );
        }
        
        if( !$this->simpleloginsecure->is_admin() ) {
            redirect( 'admin/login/' );
        }

        $this->load->model('categorii_model', 'categoriim');
    }

    public function index()
    {
        if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }

        $data['items'] = $this->categoriim->get_categorii();
		$data["page_view"] = "admin/categorii";
        $this->load->library('display', $data);
	}

    public function adauga() {

        $data["page_view"] = "admin/editare_produs";
        $this->load->library('display', $data);

    }

    public function editeaza($id) {

        $data['id'] = $id;

        $data['item'] = $this->categoriim->get_categorie($id);

        $data["page_view"] = "admin/editare_categorie";
        $this->load->library('display', $data);

    }

    public function salveaza( $id = 0 ) {

        $this->load->helper('security');
        $this->load->library('functions');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nume', 'Nume', 'trim|required');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();

            $this->db->set('nume', xss_clean($p['nume']));
            $this->db->set('slug', $this->functions->build_slug(xss_clean($p['nume'])));

            if( $id == 0) {

                $this->db->insert('categorii');

                $data['success'] = "Categoria a fost adaugata";

            } else {

                $this->db->where('id', $id);
                $this->db->update('categorii');

                $data['id'] = $id;

                $data['success'] = "Categoria a fost actualizata";
            }


        }

        $data["page_view"] = "admin/editare_categorie";
        $this->load->library('display', $data);
    }

    public function sterge($id) {

        $this->categoriim->sterge_categorie($id);

        $this->session->set_flashdata('success', 'Categoria a fost stearsa cu succes');

        redirect('admin/categorii');

    }


}
