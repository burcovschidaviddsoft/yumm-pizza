<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comenzi extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if( !$this->simpleloginsecure->is_logat() ) {
            redirect( 'admin/login/' );
        }
        
        if( !$this->simpleloginsecure->is_admin() ) {
            redirect( 'admin/login/' );
        }

        $this->load->model('categorii_model', 'categoriim');
        $this->load->model('produse_model', 'produsem');
        $this->load->model('comenzi_model', 'comenzim');
    }

    public function index() {

        if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }
        if( !empty($this->session->flashdata('error')) ) {
            $data['error'] = $this->session->flashdata('error');
        }

        $data['items'] = $this->comenzim->get_comenzi();
		
		$data["page_view"] = "admin/comenzi";
        $this->load->library('display', $data);
	}

    public function detalii($id) {

        if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }
        if( !empty($this->session->flashdata('error')) ) {
            $data['error'] = $this->session->flashdata('error');
        }

        $data['id'] = $id;

        $data['item'] = $this->comenzim->get_comanda($id);

        $data['produse'] = array();
        $produse_comanda = $this->comenzim->get_produse_comanda($id);

        $k = 0;
        $data['total'] = 0;
        foreach ($produse_comanda as $produs) {

            $data['produse'][] = $produs;
            $data['total'] += $produs['pret']*$produs['cantitate'];

            $optiuni = $this->comenzim->get_optiuni_produse_comanda($produs['id_comanda'], $produs['id_produs']);
            foreach ($optiuni as $optiune) {
                $data['produse'][$k]['op'][] = $this->produsem->get_optiune($optiune['id_optiune']);
            }

            $k++;
        }

        $data["page_view"] = "admin/detalii_comanda";
        $this->load->library('display', $data);

    }

    public function accepta( $id ) {

        $this->load->helper('security');
        $this->load->library('functions');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('durata', 'Durata', 'trim|required');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();

            #====== trimitere sms catre client
            $this->load->model('useri_model', 'userim');

            $user_id = $this->comenzim->get_comanda($id)['id_user'];
            $user = $this->userim->get_user($user_id);
            $total = $this->comenzim->get_total_comanda($id);

            /*$params = array(
                    'from' => 'client@zamira.ro',
                    'recipient' => $client->company_mobile.'@smslink.ro',
                    'subject' => 'zamira420client',
                    'message' => $texts[$k],
                );*/

            $this->load->library('email');
            $this->email->from('contact@yummpizza.ro');
            #$this->email->to($user['telefon'].'@smslink.ro');
            $this->email->to('victor.tudosa@gmail.com');
            $this->email->subject('parolaSMSlink');
            $this->email->message('Salut, '.$user['nume'].'! Comanda ta in valoare de '.$total.' lei va ajunge la tine in '.$p['durata'].'. Multumim!');

            #$this->email->send();

            $this->db->set('confirmat', 1);
            $this->db->where('id', $id);
            $this->db->update('comenzi');

            $data['id'] = $id;

            $this->session->set_flashdata('success', 'Comanda a fost trimisa');
            redirect('admin/comenzi');

        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        redirect('admin/comenzi/detalii/'.$id);
    }

    public function sterge($id) {

        $this->comenzim->sterge_comanda($id);
        $this->comenzim->sterge_produse_comanda($id);
        $this->comenzim->sterge_optiuni_produse_comanda($id);

        $this->session->set_flashdata('success', 'Comanda a fost anulata cu succes');

        redirect('admin/comenzi');

    }


}
