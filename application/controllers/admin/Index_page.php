<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_page extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if( !$this->simpleloginsecure->is_logat() ) {
            redirect( 'admin/login/' );
        }
        if( !$this->simpleloginsecure->is_admin() ) {
            redirect( 'admin/login/' );
        }

        $this->load->model('produse_model', 'produsem');
        $this->load->model('ingrediente_model', 'ingredientem');
        $this->load->model('categorii_model', 'categoriim');
        $this->load->model('optiuni_clase_model', 'clasem');
        $this->load->model('optiuni_produse_model', 'optiunim');
    }

	public function index() {

        if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }

        $items = $this->produsem->get_produse();

        $k = 0;
        foreach ($items as $item) {

            $data['items'][$k] = $item;
            $data['items'][$k]['categorie'] = $this->categoriim->get_nume_categorie($item['id_categorie']);
            
            $k++;
        }

		$data["page_view"] = "admin/index_page";
        $this->load->library('display', $data);
	}

    public function adauga() {

    	$data['select_categorie'] = $this->categoriim->drowpdown_categorii();

        //$data['ingrediente'] = $this->ingredientem->get_ingrediente();
        $data['ingrediente'] = array();
        $data['ingrediente_produs'] = array();
        
        $data["page_view"] = "admin/editare_produs";
        $this->load->library('display', $data);
    }

    public function editeaza($id) {

    	$data['id'] = $id;

    	$data['item'] = $this->produsem->get_produs($id);

    	$data['select_categorie'] = $this->categoriim->drowpdown_categorii($data['item']['id_categorie']);

        $data['ingrediente'] = $this->ingredientem->get_ingrediente_by_categorie($data['item']['id_categorie']);
        $data['ingrediente_produs'] = $this->produsem->get_ingrediente($data['id']);

        $data["page_view"] = "admin/editare_produs";
        $this->load->library('display', $data);
    }

    public function salveaza( $id = 0 ) {

    	$this->load->helper('security');
        $this->load->library('functions');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nume', 'Nume', 'trim|required');
        $this->form_validation->set_rules('descriere', 'Descriere', 'trim|required');
        $this->form_validation->set_rules('categorie', 'Categorie', 'trim|required|integer');
        $this->form_validation->set_rules('imagine', 'Imagine', 'trim');
        $this->form_validation->set_rules('ingrediente[]', 'Ingrediente', 'numeric');
        $this->form_validation->set_rules('evidentiat', 'Evidentiat pe prima pagina', 'trim|numeric');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();

            $this->db->set('nume', xss_clean($p['nume']));
            $this->db->set('slug', $this->functions->build_slug(xss_clean($p['nume'])));
            $this->db->set('descriere', xss_clean($p['descriere']));
            $this->db->set('id_categorie', xss_clean($p['categorie']));
            
            if(isset($p['evidentiat']) && !empty($p['evidentiat'])) {
                $this->db->set('evidentiat', 1);
            } else {
                $this->db->set('evidentiat', 0);
            }

            if( $id == 0) {

                $this->db->insert('produse');

                $id = $this->db->insert_id();

                $this->produsem->asociaza_ingrediente($id, $p['ingrediente']);

                $data['success'] = "Produsul a fost adaugat, nu uita sa ii editezi optiunile!";

            } else {

                $this->db->where('id', $id);
                $this->db->update('produse');

                $data['id'] = $id;

                $this->produsem->reasociaza_ingrediente($id, $p['ingrediente']);

                $data['success'] = "Produsul a fost actualizat";
            }
            if (isset($_FILES['imagine']['name']) && !empty($_FILES['imagine']['name'])) {
                $this->salveaza_imagine($id);
            }
        }
        
        $data['id'] = $id;
        $data['item'] = $this->produsem->get_produs($id);
    	$data['select_categorie'] = $this->categoriim->drowpdown_categorii(xss_clean($data['item']['id_categorie']));
        $data['ingrediente'] = $this->ingredientem->get_ingrediente_by_categorie(xss_clean($data['item']['id_categorie']));
        $data['ingrediente_produs'] = $this->produsem->get_ingrediente($data['id']);
        $data["page_view"] = "admin/editare_produs";
        $this->load->library('display', $data);
    }

    private function salveaza_imagine( $id_produs ) {
        
		$this->load->helper('security');

        $config['upload_path'] = 'application/views/images/produse/';
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);

        $_FILES['imagine'] = xss_clean($_FILES['imagine'], TRUE);

        if (isset($_FILES['imagine']['name']) && !empty($_FILES['imagine']['name'])) {

            $this->sterge_imagine($id_produs);

            if (!$this->upload->do_upload('imagine')) {

                $data['error'] = $this->upload->display_errors();

            } else {

                $img = array('upload_data' => $this->upload->data());

                $this->db->set('image', $img['upload_data']['file_name']);
                $this->db->where('id', $id_produs);
                $this->db->update('produse');
            }
        }
    }

    private function sterge_imagine( $id_produs ) {

        $imagine = $this->produsem->get_produs($id_produs)['image'];

        if( isset($imagine) && !empty($imagine)) {
            unlink('application/views/images/produse/'.$imagine);
        }

    }

    public function sterge($id) {

        $this->produsem->sterge_produs($id);

        $this->session->set_flashdata('success', 'Produsul a fost sters cu succes');

        redirect('admin');
    }


    public function optiuni($id) {

        if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }

        $data['id'] = $id;
        $data['items'] = $this->produsem->get_optiuni_produs($id);

        $data["page_view"] = "admin/optiuni_produs";
        $this->load->library('display', $data);
    }

    public function adauga_optiune($id) {

        $data['id_produs'] = $id;
        $data['optiuni_clase'] = $this->clasem->drowpdown_optiuni(0, $this->produsem->get_produs($id)['id_categorie']);
        $data["page_view"] = "admin/editare_optiune";
        $this->load->library('display', $data);
    }

    public function editeaza_optiune($id_produs, $id) {

        $data['id'] = $id;
        $data['id_produs'] = $id_produs;
        $data['item'] = $this->produsem->get_optiune($id);
        $data['optiuni_clase'] = $this->clasem->drowpdown_optiuni($data['item']['id_clasa_optiuni'], $this->produsem->get_produs($id_produs)['id_categorie']);
        $data["page_view"] = "admin/editare_optiune";
        $this->load->library('display', $data);
    }

    public function salveaza_optiune( $id_produs, $id = 0 ) {

        $this->load->helper('security');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nume', 'Nume', 'trim|required');
        $this->form_validation->set_rules('clasa', 'Clasa', 'numeric|required');
        $this->form_validation->set_rules('pret', 'Pret', 'numeric|required');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();

            $this->db->set('nume', xss_clean($p['nume']));
            $this->db->set('id_produs', xss_clean($id_produs));

            if( $id == 0) {

                $this->optiunim->insert_optiune(array(
                    "nume"=>xss_clean($p["nume"]),
                    "id_produs"=>xss_clean($id_produs),
                    "id_clasa_optiuni"=>xss_clean($p["clasa"]),
                    "pret"=>xss_clean($p["pret"])
                ));

                $data['success'] = "Optiunea a fost adaugata";

            } else {

                $this->optiunim->update_optiune($id, array(
                    "nume"=>xss_clean($p["nume"]),
                    "id_produs"=>xss_clean($id_produs),
                    "id_clasa_optiuni"=>xss_clean($p["clasa"]),
                    "pret"=>xss_clean($p["pret"])
                ));
                $data['success'] = "Optiunea a fost actualizata";
            }            

        }
        
        $data['id'] = $id;
        $data['id_produs'] = $id_produs;
        $data['item'] = $this->produsem->get_optiune($id);
        $data['optiuni_clase'] = $this->clasem->drowpdown_optiuni($data['item']['id_clasa_optiuni'], $this->produsem->get_produs($id_produs)['id_categorie']);
        $data["page_view"] = "admin/editare_optiune";
        $this->load->library('display', $data);
    }

    public function sterge_optiune($id_produs, $id) {

        $this->produsem->sterge_optiune($id);

        $this->session->set_flashdata('success', 'Optiunea a fost stearsa cu succes');

        redirect('admin/index_page/optiuni/'.$id_produs);
    }
}
