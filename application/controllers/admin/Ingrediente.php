<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingrediente extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if( !$this->simpleloginsecure->is_logat() ) {
            redirect( 'admin/login/' );
        }
        
        if( !$this->simpleloginsecure->is_admin() ) {
            redirect( 'admin/login/' );
        }

        $this->load->model('ingrediente_model', 'ingredientem');
        $this->load->model('ingrediente_categorii_model', 'ingrediente_categoriim');
        $this->load->model('categorii_model', 'categoriim');
    }

    public function index()
    {
        if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }

        $data['items'] = $this->ingredientem->get_ingrediente();
		
		$data["page_view"] = "admin/ingrediente";
        $this->load->library('display', $data);
	}

    public function adauga() {
        $data['categorii'] = $this->categoriim->get_categorii();
        $data["page_view"] = "admin/editare_ingrediente";
        $this->load->library('display', $data);

    }

    public function editeaza($id) {

        $data['id'] = $id;
        $data['categorii'] = $this->categoriim->get_categorii();
        $data['categorii_ingredient'] = $this->ingredientem->get_categorii_ingredient($id);
        $data['item'] = $this->ingredientem->get_ingredient($id);
        $data["page_view"] = "admin/editare_ingrediente";
        $this->load->library('display', $data);

    }

    public function salveaza( $id = 0 ) {

        $this->load->helper('security');
        $this->load->library('functions');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nume', 'Nume', 'trim|required');
        $this->form_validation->set_rules('pret_extra', 'Pret pentru extra portie', 'numeric|required');
        $this->form_validation->set_rules('categorii[]', 'Pret pentru extra portie', 'numeric');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();

            if( $id == 0) {

                $this->ingredientem->insert_ingredient(array(
                    "nume"=>xss_clean($p['nume']),
                    "pret_extra"=>xss_clean($p['pret_extra'])
                ));

                $this->ingrediente_categoriim->reseteaza_relatii($this->db->insert_id(), $p["categorii"]);

                $data['success'] = "Ingredientul a fost adaugat";

            } else {

                $this->ingredientem->update_ingredient($id, array(
                    "nume"=>xss_clean($p['nume']),
                    "pret_extra"=>xss_clean($p['pret_extra'])
                ));

                $data['id'] = $id;

                $this->ingrediente_categoriim->reseteaza_relatii($id, $p["categorii"]);

                $data['success'] = "Ingredientul a fost actualizat";
            }


        }
        $data['categorii'] = $this->categoriim->get_categorii();
        if($id === 0)
            $data['categorii_ingredient'] = $this->ingredientem->get_categorii_ingredient($this->db->insert_id());
        else
            $data['categorii_ingredient'] = $this->ingredientem->get_categorii_ingredient($id);
        $data["page_view"] = "admin/editare_ingrediente";
        $this->load->library('display', $data);
    }

    public function sterge($id) {

        $this->ingredientem->sterge_ingredient($id);

        $this->session->set_flashdata('success', 'Ingredientul a fost sters cu succes');

        redirect('admin/ingrediente');

    }


}
