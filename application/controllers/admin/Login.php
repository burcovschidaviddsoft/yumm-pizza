<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index( $data = array() )
	{

		if( $this->session->flashdata('succes')!="" ) {
            $data['succes'] = $this->session->flashdata('succes');
        }


		$data["page_view"] = "admin/login";
        $this->load->library('display', $data);
	}

	public function verificare() {
        $data = array();

		if( $this->simpleloginsecure->login( $this->input->post('user_email_login'), $this->input->post('user_pass_login'), 1 ) ) {

			redirect('admin');

		} else {
            $data["errorLogin"] = "Adresa de email sau parola gresita.";
        }

        $this->index($data);

	}

	function logout() {
        $this->simpleloginsecure->logout();
        redirect();
    }
}
