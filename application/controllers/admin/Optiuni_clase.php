<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Optiuni_clase extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if( !$this->simpleloginsecure->is_logat() ) {
            redirect( 'admin/login/' );
        }
        
        if( !$this->simpleloginsecure->is_admin() ) {
            redirect( 'admin/login/' );
        }

        $this->load->model('categorii_model', 'categoriim');
        $this->load->model('optiuni_produse_model', 'optiuni_produsem');
        $this->load->model('optiuni_clase_model', 'optiuni_clasem');
    }

    public function index()
    {
        if( !empty($this->session->flashdata('success')) ) {
            $data['success'] = $this->session->flashdata('success');
        }

        $data['items'] = $this->optiuni_clasem->get_clase();
		$data["page_view"] = "admin/optiuni_clase";
        $this->load->library('display', $data);
	}

    public function adauga() {

        $data['optiuni_select'] = $this->categoriim->drowpdown_categorii();
        $data["page_view"] = "admin/editare_optiuni_clase";
        $this->load->library('display', $data);

    }

    public function editeaza($id) {

        $data['id'] = $id;
        $data['item'] = $this->optiuni_clasem->get_clasa($id);
        $data['optiuni_select'] = $this->categoriim->drowpdown_categorii($data['item']['id']);
        $data["page_view"] = "admin/editare_optiuni_clase";
        $this->load->library('display', $data);

    }

    public function salveaza( $id = 0 ) {

        $this->load->helper('security');
        $this->load->library('functions');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nume', 'Nume', 'trim|required');
        $this->form_validation->set_rules('categorie', 'Categorie', 'numeric|required');

        if($this->form_validation->run() == TRUE) {

            $p = $this->input->post();          

            if( $id == 0) {

                $this->optiuni_clasem->insert_clasa(array(
                    "nume"=>xss_clean($p["nume"]),
                    "id_categorie"=>xss_clean($p["categorie"]) 
                ));

                $data['success'] = "Categoria de optiuni a fost adaugata";

            } else {

                $this->optiuni_clasem->update_clasa($id, array(
                    "nume"=>xss_clean($p["nume"]),
                    "id_categorie"=>xss_clean($p["categorie"]) 
                ));

                $data['id'] = $id;

                $data['success'] = "Categoria de optiuni a fost actualizata";
            }


        }

        $data['optiuni_select'] = $this->categoriim->drowpdown_categorii($p["categorie"]);
        $data["page_view"] = "admin/editare_optiuni_clase";
        $this->load->library('display', $data);
    }

    public function sterge($id) {

        $this->optiuni_clasem->sterge_clasa($id);

        $this->session->set_flashdata('success', 'Categoria a fost stearsa cu succes');

        redirect('admin/optiuni_clase');

    }


}
