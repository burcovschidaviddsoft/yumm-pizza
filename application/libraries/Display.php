<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Display {

	public function __construct($data)
	{
		self::display($data);
	}

	public function display( $data ) {

		$CI =& get_instance();

		$CI->load->model('categorii_model', 'categoriim');
		$CI->load->model('cos_model', 'cosm');

		$data['categorii'] = $CI->categoriim->get_categorii();

		if( !empty($CI->session->userdata('cos')) ) {

			$data['total_cos'] = $CI->cosm->get_total();
			$data['items_cos'] = $CI->cosm->get_total_produse();

		} else {
			$data['total_cos'] = 0;
			$data['items_cos'] = 0;
		}

		$CI->load->view('header', $data);
        $CI->load->view($data['page_view'], $data);
        $CI->load->view('footer', $data);

	}

}