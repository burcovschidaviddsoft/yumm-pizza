<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Functions {

	function build_slug( $slug ) {
        $slug = preg_replace('/[^\da-z ]/i', '', $slug);
        
        return url_title( strtolower( trim( $slug ) ) );
    }

}