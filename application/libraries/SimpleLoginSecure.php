<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SimpleLoginSecure Class
 * @package   SimpleLoginSecure
 * @version   1.0.1
 * @author    Alex Dunae, Dialect <alex[at]dialect.ca>
 * @copyright Copyright (c) 2008, Alex Dunae
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt
 * @link      http://dialect.ca/code/ci-simple-login-secure/
 */
class SimpleLoginSecure {
    var $CI;
    var $user_table = 'useri';

    function check_email_db( $user_email ) {
        $this->CI =& get_instance();

        //Check against user table
        $this->CI->db->where('user_email', $user_email);
        $query = $this->CI->db->get_where($this->user_table);

        if ($query->num_rows() > 0) //user_email already exists
            return true;

        return false;
    }
    
    function check_email_db_user( $user_email ) {
        $this->CI =& get_instance();

        $sessionUserEmail = $this->CI->session->userdata('user_email');

        //Check against user table
        $this->CI->db->where('user_email', $user_email);
        $this->CI->db->where('user_email <>', $sessionUserEmail );
        $query = $this->CI->db->get_where($this->user_table);

        if ($query->num_rows() > 0) //user_email already exists
            return true;

        return false;
    }

    function check_email_db_user_edit( $user_email ) {
        $this->CI =& get_instance();

        $sessionUserEmail = $this->CI->session->userdata('user_email');

        //Check against user table
        $this->CI->db->where('user_email', $user_email);
        $this->CI->db->where('user_email <>', $sessionUserEmail );
        $query = $this->CI->db->get_where($this->user_table);

        if ($query->num_rows() > 0) //user_email already exists
            return true;

        return false;
    }

    /**
     * Login and sets session variables
     *
     * @access	public
     * @param	string
     * @param	string
     * @return	bool
     */
    function login($user_email = '', $user_pass = '', $admin = 0, $temp_login = false) {
        $this->CI =& get_instance();

        if($user_email == '' OR $user_pass == '')
            return false;

        //Check if already logged in
        if($this->CI->session->userdata('login')['user_email'] == $user_email)
            return true;
        
        //Check against user table
        $this->CI->db->where('user_email', $user_email);

        if( $admin==1 ) {
            $this->CI->db->where('(tip="1")', "", FALSE);
        }
        
        $query = $this->CI->db->get_where($this->user_table);

        if ($query->num_rows() > 0) {
            $user_data = $query->row_array();
            
            if( md5( $user_pass )!=$user_data['user_pass'] )
                return false;

            /*//Destroy old session
            $this->CI->session->sess_destroy();

            //Create a fresh, brand new session
            $this->CI->session->sess_create();*/
            /*$this->CI->session->unset_userdata('user_email');
            $this->CI->session->unset_userdata('user');
            $this->CI->session->unset_userdata('logged_in');
            $this->CI->session->unset_userdata('user');*/
            $this->CI->session->unset_userdata('login');

            //Set session data
            unset($user_data['user_pass']);
            $user_data['user'] = $user_data['user_email']; // for compatibility with Simplelogin
            $user_data['logged_in'] = true;

            $this->CI->session->set_userdata('login', $user_data);

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Logout user
     *
     * @access	public
     * @return	void
     */
    function logout() {
        $this->CI =& get_instance();

        /*$this->CI->session->unset_userdata('user_email');
        $this->CI->session->unset_userdata('user');
        $this->CI->session->unset_userdata('logged_in');*/
        $this->CI->session->unset_userdata('login');
    }

    function is_logat() {
        $this->CI =& get_instance();

        $user_email = $this->CI->session->userdata('login')['user_email'];
        if( $user_email!="" ) {
            $this->CI->db->where("user_email", $user_email);
            $query = $this->CI->db->get( $this->user_table );
            
            return $query->num_rows();
        }
        
        
        return false;
    }

    function is_admin() {
        if( $this->is_logat() ) {
            return $this->CI->session->userdata('login')['tip']=='1' ? TRUE : FALSE;
        }
        
        return false;
    }

    function check_user_pass( $user_pass ) {
        $this->CI =& get_instance();

        $this->CI->db->where("id", $this->CI->session->userdata('login')['id']);
        $query = $this->CI->db->get( $this->user_table );
        if( $query->num_rows()>0 ) {
            $user_data = $query->row_array();
            if( md5($user_pass)==$user_data['user_pass'] )
                return true;
        }

        return false;
    }

    function update_user_pass( $user_id, $user_pass ) {
        $this->CI =& get_instance();

        $this->CI->db->set("user_pass", md5($user_pass));
        $this->CI->db->where("id", $user_id);
        if( $this->CI->db->update( $this->user_table ) ) {
            return true;
        }

        return false;
    }

}

/* End of file SimpleLoginSecure.php */
/* Location: ./application/libraries/SimpleLoginSecure.php */
