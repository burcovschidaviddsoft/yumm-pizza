<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Categorii_model extends CI_Model {

	public function get_categorii() {

		$this->db->select('*');

		$this->db->from('categorii');

		return $this->db->get()->result_array();

	}

	public function get_categorie($id) {

		$this->db->select('*');

		$this->db->where('id', $id);

		$this->db->from('categorii');

		return $this->db->get()->row_array();

	}

	public function get_categorie_by_slug($slug) {

		$this->db->select('*');

		$this->db->where('slug', $slug);

		$this->db->from('categorii');

		return $this->db->get()->row_array();

	}

	public function sterge_categorie($id) {

		$this->db->where('id', $id);

		$this->db->delete('categorii');

	}

	public function get_nume_categorie($id) {

		$this->db->select('nume');

		$this->db->from('categorii');

		$this->db->where('id', $id);

		return $this->db->get()->row()->nume;

	}

	public function drowpdown_categorii( $id = 0 ) {

		$categorii = $this->get_categorii();

		$select = "<select name='categorie' class='form-control'>";
		$select .= "<option>Selectati categoria</option>";

		foreach ($categorii as $categorie) {

			$checked = "";
			if($categorie['id'] == $id)
				$checked = "selected";
			
			$select .= "<option value='{$categorie['id']}' {$checked}>{$categorie['nume']}</option>";

		}

		$select .= "</select>";

		return $select;

	}

}