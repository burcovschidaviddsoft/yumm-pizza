<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Comenzi_model extends CI_Model {

	public function get_comenzi() {

		$this->db->select('*');

		$this->db->from('comenzi');

		$this->db->where('confirmat', 0);

		$this->db->order_by('id', 'desc');

		return $this->db->get()->result_array();

	}

	public function get_comanda($id) {

		$this->db->select('*');

		$this->db->where('id', $id);

		$this->db->from('comenzi');

		return $this->db->get()->row_array();

	}

	public function get_produse_comanda($id) {

		$this->db->select('a.*, b.nume');

		$this->db->from('produse_comanda a');

		$this->db->join("produse b", "a.id_produs = b.id");

		$this->db->where('a.id_comanda', $id);

		return $this->db->get()->result_array();

	}

	public function get_optiuni_produse_comanda($id_comanda, $id_produs) {

		$this->db->select('*');

		$this->db->from('optiuni_produse_comanda a');

		$this->db->where('id_comanda', $id_comanda);

		$this->db->where('id_produs', $id_produs);

		return $this->db->get()->result_array();
	}

	public function get_total_comanda($id) {

		$this->db->select('*');

		$this->db->from('produse_comanda');

		$this->db->where('id_comanda', $id);

		$produse = $this->db->get()->result_array();

		$total = 0;

		foreach ($produse as $produs) {
			$total += $produs['pret']*$produs['cantitate'];
		}

		return $total;

	}

	public function sterge_comanda($id) {

		$this->db->where('id', $id);

		$this->db->delete('comenzi');
	}

	public function sterge_produse_comanda($id_comanda) {

		$this->db->where('id_comanda', $id_comanda);

		$this->db->delete('produse_comanda');

	}

	public function sterge_optiuni_produse_comanda($id_comanda) {

		$this->db->where('id_comanda', $id_comanda);

		$this->db->delete('optiuni_produse_comanda');

	}

}