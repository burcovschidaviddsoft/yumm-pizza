<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Cos_model extends CI_Model {

	public function get_total() {

		$total = 0;
		foreach ($this->session->userdata('cos') as $produs) {
			$total += $produs['pret']*$produs['cantitate'];
		}
		return $total;
	}

	public function get_total_produse() {

		$total = 0;
		foreach ($this->session->userdata('cos') as $produs) {
			$total += $produs['cantitate'];
		}
		return $total;
	}


}