<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Ingrediente_categorii_model extends CI_Model {

	public function insert_relatie($data)
	{
		$this->db->insert('ingrediente_categorii', $data);
	}

	public function sterge_relatie($id)
	{
		$this->db->where('id', $id);

		$this->db->delete("ingrediente_categorii");
	}

	public function reseteaza_relatii($id_ingredient, $categorii)
	{
		$this->db->where('id_ingredient', $id_ingredient)->delete('ingrediente_categorii');
		foreach($categorii as $categorie)
			$this->insert_relatie(array(
				"id_ingredient"=>$id_ingredient,
				"id_categorie"=>$categorie
			));
	}

}