<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Ingrediente_model extends CI_Model {

	public function get_ingredient($id)
	{
		$this->db->select('*');

		$this->db->from('ingrediente');

		$this->db->where("id", $id);

		return $this->db->get()->result_array()[0];
	}

	public function get_ingrediente_by_categorie($id)
	{

		$this->db->select('id_ingredient');

		$this->db->from('ingrediente_categorii');

		$this->db->where("id_categorie", $id);

		$ingrediente = $this->db->get()->result_array();
		$id_ingrediente = array(-1);
		foreach($ingrediente as $ingredient)
			array_push($id_ingrediente, $ingredient['id_ingredient']);

		return $this->db->select('*')->from('ingrediente')->where_in('id', $id_ingrediente)->get()->result_array();
	}

	public function get_categorii_ingredient($id)
	{
		$this->db->select('id_categorie');

		$this->db->from('ingrediente_categorii');

		$this->db->where("id_ingredient", $id);

		$ingrediente = $this->db->get()->result_array();
		$id_ingrediente = array(-1);
		foreach($ingrediente as $ingredient)
			array_push($id_ingrediente, $ingredient['id_categorie']);

		return $this->db->select('*')->from('categorii')->where_in('id', $id_ingrediente)->get()->result_array();		
	}

	public function get_ingrediente()
	{
		$this->db->select('*');

		$this->db->from('ingrediente');

		return $this->db->get()->result_array();
	}

	public function insert_ingredient($data)
	{
		$this->db->insert('ingrediente', $data);
	}

	public function sterge_ingredient($id)
	{
		$this->db->where('id', $id);

		$this->db->delete("ingrediente");
	}

	public function update_ingredient($id, $data)
	{
		$this->db->set($data);
		$this->db->where("id", $id);
		$this->db->update("ingrediente", $data);	
	}

}