<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Optiuni_clase_model extends CI_Model {

	public function get_clasa($id)
	{
		$this->db->select('*');

		$this->db->from('clase_optiuni');

		$this->db->where("id", $id);

		return $this->db->get()->result_array()[0];
	}

	public function get_clase()
	{
		$this->db->select('*');

		$this->db->from('clase_optiuni');

		return $this->db->get()->result_array();
	}

	public function insert_clasa($data)
	{
		$this->db->insert('clase_optiuni', $data);
	}

	public function sterge_clasa($id)
	{
		$this->db->where('id', $id);

		$this->db->delete("clase_optiuni");
	}

	public function update_clasa($id, $data)
	{
		$this->db->set($data);
		$this->db->where("id", $id);
		$this->db->update("clase_optiuni", $data);	
	}

	public function drowpdown_optiuni( $id = 0, $id_categorie = 0 ) {

		if($id_categorie === 0)
			$categorii = $this->get_clase();
		else
		{
			$this->db->select('*');
			$this->db->from('clase_optiuni');
			$this->db->where("id_categorie", $id_categorie);
			$categorii = $this->db->get()->result_array();

		}
		$select = "<select name='clasa' class='form-control'>";
		$select .= "<option>Selectati clasa de optiuni</option>";

		foreach ($categorii as $categorie) {

			$checked = "";
			if($categorie['id'] == $id)
				$checked = "selected";
			
			$select .= "<option value='{$categorie['id']}' {$checked}>{$categorie['nume']}</option>";

		}

		$select .= "</select>";

		return $select;

	}

}