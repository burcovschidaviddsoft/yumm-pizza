<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Optiuni_produse_model extends CI_Model {

	public function get_optiuni_produs($id)
	{
		$this->db->select('*');

		$this->db->from('optiuni_produse');

		$this->db->where("id_produs", $id);

		return $this->db->get()->result_array()[0];
	}

	public function get_optiuni()
	{
		$this->db->select('*');

		$this->db->from('optiuni_produse');

		return $this->db->get()->result_array();
	}

	public function insert_optiune($data)
	{
		$this->db->insert('optiuni_produse', $data);
	}

	public function sterge_optiune($id)
	{
		$this->db->where('id', $id);

		$this->db->delete("optiuni_produse");
	}

	public function update_optiune($id, $data)
	{
		$this->db->set($data);
		$this->db->where("id", $id);
		$this->db->update("optiuni_produse", $data);	
	}

}