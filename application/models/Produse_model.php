<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Produse_model extends CI_Model {

	public function get_produse_categorie($id_categorie) {

		$this->db->select('*');

		$this->db->from('produse');

		$this->db->where('id_categorie', $id_categorie);

		return $this->db->get()->result_array();

	}

	public function get_produse_evidentiate() {

		$this->db->select('*');

		$this->db->from('produse');

		$this->db->where('evidentiat', 1);

		return $this->db->get()->result_array();

	}

	public function get_produse() {

		$this->db->select('*');

		$this->db->from('produse');

		return $this->db->get()->result_array();

	}

	public function get_produs($id) {

		$this->db->select('*');

		$this->db->where('id', $id);

		$this->db->from('produse');

		return $this->db->get()->row_array();

	}

	public function sterge_produs($id) {

		$this->db->where('id', $id);

		$this->db->delete('produse');

	}

	public function get_optiuni_produs($id_produs) {

		$this->db->select('*');

		$this->db->where('id_produs', $id_produs);

		$this->db->from('optiuni_produse');

		return $this->db->get()->result_array();

	}

	public function get_optiuni_pe_clase($id_produs) {

		$id_categorie = $this->get_produs($id_produs)["id_categorie"];
		$clase_optiuni = $this->db->select('*')->from('clase_optiuni')->where('id_categorie', $id_categorie)->get()->result_array();

		foreach($clase_optiuni as $k=>$clasa_optiuni)
		{
			$clase_optiuni[$k]["optiuni"] = $this->db->select('*')->from('optiuni_produse')->where('id_produs', $id_produs)->where('id_clasa_optiuni', $clasa_optiuni['id'])->get()->result_array();
		}
		return $clase_optiuni;

	}

	public function get_optiune($id) {

		$this->db->select('*');

		$this->db->where('id', $id);

		$this->db->from('optiuni_produse');

		return $this->db->get()->row_array();

	}

	public function sterge_optiune($id) {

		$this->db->where('id', $id);

		$this->db->delete('optiuni_produse');

	}

	public function asociaza_ingredient($id_ingredient, $id_produs)
	{
		$this->db->insert('produse_ingrediente', array(
			"id_ingredient"=>$id_ingredient,
			"id_produs"=>$id_produs
		));
	}

	public function deasociaza_ingredient($id_ingredient, $id_produs)
	{
		$this->db->where("id_ingredient", $id_ingredient);
		$this->db->where("id_produs", $id_produs);
		$this->db->delete('produse_ingrediente');
	}

	public function get_ingrediente($id)
	{
		$this->db->select('id_ingredient');
		$this->db->from('produse_ingrediente');
		$this->db->where('id_produs', $id);

		$id_igrediente_asociate = $this->db->get()->result_array();
		$this->db->select('*');
		$this->db->from('ingrediente');

		$id_ingrediente = array(-1);
		foreach($id_igrediente_asociate as $id_ingredient)
		{
			array_push($id_ingrediente, $id_ingredient['id_ingredient']);
		}

		$this->db->where_in("id", $id_ingrediente);
		return $this->db->get()->result_array();
	}

	public function adauga_optiune($id_produs, $id_clasa, $pret, $nume)
	{
		$this->db->insert("optiuni_produse", array(
			"nume"=>$nume,
			"id_produs"=>$id_produs,
			"id_clasa_optiuni"=>$id_clasa,
			"pret"=>$pret
		));
	}

	public function asociaza_ingrediente($id_produs, $ingrediente)
	{
		for($i = 0; $i < count($ingrediente); $i = $i + 1)
			$this->asociaza_ingredient($ingrediente[$i], $id_produs);
	}

	public function reasociaza_ingrediente($id_produs, $ingrediente)
	{
		$ingrediente_actuale = $this->get_ingrediente($id_produs);

		for($i = 0; $i < count($ingrediente_actuale); $i = $i + 1)
			$this->deasociaza_ingredient($ingrediente_actuale[$i]['id'], $id_produs);

		for($i = 0; $i < count($ingrediente); $i = $i + 1)
			$this->asociaza_ingredient($ingrediente[$i], $id_produs);
	}

}