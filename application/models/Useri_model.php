<?php if( !defined( 'BASEPATH' ) ) exit('No direct script access allowed');

class Useri_model extends CI_Model {

	public function get_useri() {

		$this->db->select('*');

		$this->db->from('useri');

		$this->db->order_by('id', 'desc');

		return $this->db->get()->result_array();

	}

	public function get_user($id) {

		$this->db->select('*');

		$this->db->where('id', $id);

		$this->db->from('useri');

		return $this->db->get()->row_array();

	}

}