<div id="content" class="site-content" tabindex="-1" >
   <div class="col-full" style="margin-top: 20px;">
      <div class="row">
         <div class="col-md-3">
            <?php $this->load->view('admin/sidebar'); ?>
         </div>  
         <div class="col-md-9">
            <h2>Detalii comanda</h2>
            <?php if( isset( $success ) ): ?>
               <div class="alert alert-success alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
              </div>
            <?php endif; ?>

            <?php if( isset( $error ) ): ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
              </div>
            <?php endif; ?>

            <?php if( validation_errors()!="" ): ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= validation_errors() ?></div>
              </div>
            <?php endif; ?>
            <p><strong>Adresa: </strong><?= $item['adr'] ?> </p>
            <p><strong>Detalii: </strong><?= $item['detalii'] !== "" ? $item['detalii'] : "-"?> </p>
            <p><strong>Plata: </strong><?= $item['plata'] == 1 ? "cash" : "card" ?> </p>

            <table class="table table-bordered">
                  <thead>
                     <tr>
                       <th class="col-md-6">Nume</th>
                       <th class="col-md-2">Marime</th>
                       <th class="col-md-2">Cantitate</th>
                       <th class="col-md-2">Pret</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($produse as $produs): ?>
                      <tr>
                        <td>
                          <strong><?= $produs['nume'] ?></strong><br>
                          <?php if(isset($produs['op']) && !empty($produs['op'])): ?>
                            <?php foreach ($produs['op'] as $optiune): ?>
                              <?= $optiune['nume'] ?>,
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </td>
                        <td><?= $produs['marime'] ?></td>
                        <td><?= $produs['cantitate'] ?></td>
                        <td><?= $produs['pret']*$produs['cantitate'] ?> lei</td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
               </table>

            <form method="post" action="<?= site_url('admin/comenzi/accepta/').$id ?>">
              <p class="form-row form-row-wide">
                 <label for="user_email_login">Durata<span class="required">*</span></label>
                 <input type="hidden" name="id_comanda" value="<?=$id?>">
                 <input type="hidden" name="pret" value="<?=$total?>">
                 <input type="text" class="input-text" required name="durata" value="<?=isset( $_POST['durata'] ) ? set_value('durata') : (isset($item['durata']) ? $item['durata'] : "")?>" />
              </p>
              <p class="form-row">
                 <input type="submit" class="button" name="login" value="Accepta Comanda" />
              </p>
           </form>
         </div>  
      </div>
   </div>
</div>
               