<div id="content" class="site-content" tabindex="-1" >
   <div class="col-full" style="margin-top: 20px;">
      <div class="row">
         <div class="col-md-3">
            <?php $this->load->view('admin/sidebar'); ?>
         </div>  
         <div class="col-md-9">
            <h2><?= !empty($id) ? "Editare" : "Adaugare" ?> ingredient</h2>
            <?php if( isset( $success ) ): ?>
               <div class="alert alert-success alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
              </div>
            <?php endif; ?>

            <?php if( isset( $error ) ): ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
              </div>
            <?php endif; ?>

            <?php if( validation_errors()!="" ): ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= validation_errors() ?></div>
              </div>
            <?php endif; ?>

            <form method="post" action="<?= !empty($id) ? site_url('admin/ingrediente/salveaza/').$id : site_url('admin/ingrediente/salveaza') ?>">
              <p class="form-row form-row-wide">
                 <label for="user_email_login">Nume<span class="required">*</span></label>
                 <input type="text" class="input-text" required name="nume" value="<?=isset( $_POST['nume'] ) ? set_value('nume') : (isset($item['nume']) ? $item['nume'] : "")?>" /><br>
                 <label for="user_email_login">Pret pentru extra portie<span class="required">*</span></label>
                 <input type="number" step="0.1" class="input-text" required name="pret_extra" value="<?=isset( $_POST['pret_extra'] ) ? set_value('pret_extra') : (isset($item['pret_extra']) ? $item['pret_extra'] : "2.5")?>" /><br>
                 <label for="user_email_login">Categoriile la care se aplica<span class="required">*</span></label>
                 <input type="hidden" name="categorii[]" value="-1">
                 <?php foreach($categorii as $categorie): ?>
                  <label for="user_email_login">
                    
                    <?php 
                      $checked = false;
                      if(isset($categorii_ingredient))
                      {
                        foreach($categorii_ingredient as $ci){
                          if($ci['id'] === $categorie['id'])
                          {
                            $checked = true;
                            break;
                          }
                        }
                      } 
                    ?>
                    <?php if($checked):?>
                      <input type="checkbox" name="categorii[]" checked="checked" value="<?=$categorie['id']?>">
                    <?php else:?>
                      <input type="checkbox" name="categorii[]" value="<?=$categorie['id']?>">
                    <?php endif; ?>
                     <?= $categorie['nume'] ?> </label>
                 <?php endforeach;?>
              </p>
              <p class="form-row">
                 <input type="submit" class="button" name="login" value="Salveaza" />
              </p>
           </form>
         </div>  
      </div>
   </div>
</div>
               