<div id="content" class="site-content" tabindex="-1" >
   <div class="col-full" style="margin-top: 20px;">
      <div class="row">
         <div class="col-md-3">
            <?php $this->load->view('admin/sidebar'); ?>
         </div>  
         <div class="col-md-9">
            <h2><?= !empty($id) ? "Editare" : "Adaugare" ?> clasa de optiuni</h2>
            <?php if( isset( $success ) ): ?>
               <div class="alert alert-success alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
              </div>
            <?php endif; ?>

            <?php if( isset( $error ) ): ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
              </div>
            <?php endif; ?>

            <?php if( validation_errors()!="" ): ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= validation_errors() ?></div>
              </div>
            <?php endif; ?>

            <form method="post" action="<?= !empty($id) ? site_url('admin/optiuni_clase/salveaza/').$id : site_url('admin/optiuni_clase/salveaza/') ?>">
              <p class="form-row form-row-wide">
                 <label for="user_email_login">Nume<span class="required">*</span></label>
                 <input type="text" class="input-text" required name="nume" value="<?=isset( $_POST['nume'] ) ? set_value('nume') : (isset($item['nume']) ? $item['nume'] : "")?>" /><br><br>
                 <label for="user_email_login">Categorie<span class="required">*</span></label>
                 <?= $optiuni_select ?>
              </p>
              <p class="form-row">
                 <input type="submit" class="button" name="login" value="Salveaza" />
              </p>
           </form>
         </div>  
      </div>
   </div>
</div>
               