<div id="content" class="site-content" tabindex="-1" >
   <div class="col-full" style="margin-top: 20px;">
      <div class="row">
         <div class="col-md-3">
            <?php $this->load->view('admin/sidebar'); ?>
         </div>  
         <div class="col-md-9">
            <h2><?= !empty($id) ? "Editare" : "Adaugare" ?> produs</h2>
            <?php if( isset( $success ) ): ?>
               <div class="alert alert-success alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
              </div>
            <?php endif; ?>

            <?php if( isset( $error ) ): ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
              </div>
            <?php endif; ?>

            <?php if( validation_errors()!="" ): ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= validation_errors() ?></div>
              </div>
            <?php endif; ?>

            <form method="post" action="<?= !empty($id) ? site_url('admin/index_page/salveaza/').$id : site_url('admin/index_page/salveaza') ?>" enctype="multipart/form-data">
              <p class="form-row form-row-wide">
                <label for="user_email_login">Nume<span class="required">*</span></label>
                <input type="text" class="input-text" required name="nume" placeholder="Pizza capriciosa" value="<?=isset( $_POST['nume'] ) ? set_value('nume') : (isset($item['nume']) ? $item['nume'] : "")?>" />
              </p>
              <p class="form-row form-row-wide">
                <label for="user_email_login">Descriere<span class="required">*</span></label>
                <textarea class="ckeditor" class="input-text" required name="descriere"><?=isset( $_POST['descriere'] ) ? set_value('descriere') : (isset($item['descriere']) ? $item['descriere'] : "")?></textarea>
              </p>
              <p class="form-row form-row-wide">
                <label for="user_email_login">Categorie<span class="required">*</span></label>
                <?= $select_categorie ?>
              </p>
              <p class="form-row form-row-wide">
                <div class="row">
                  <div class="col-sm-6">
                    <label for="user_email_login">Imagine</label>
                    <small>Tipuri acceptate: .gif, .jpg, .png</small>
                    <input type="file" name="imagine">
                  </div>
                  <div class="col-sm-6">
                    <?php if(isset($item['image']) && !empty($item['image'])): ?>
                      <img class="image-responsive img_thumb" src="<?= base_url().'application/views/images/produse/'.$item['image']?>">
                    <?php endif; ?>
                  </div>
                </div>
              </p>
              <div class="row">
                <div class="col-md-4">
                  <p class="form-row form-row-wide">
                    <label for="user_email_login">Ingrediente</label>
                    <input type="hidden" name="ingrediente[]" value="">
                    <?php foreach($ingrediente as $ingredient):?>

                      <?php 
                        $gasit = false;
                        foreach($ingrediente_produs as $ingredient_p)
                        {
                          if($ingredient_p === $ingredient)
                          {
                            $gasit = true;
                            break;
                          }
                        }
                      ?>

                      <?php if($gasit): ?>
                        <label class="optiuni-text"><input type="checkbox" name="ingrediente[]" checked="checked" value="<?php echo $ingredient['id'];?>" class="optiuni-text"> <?php echo $ingredient['nume']?></label>
                      <?php else: ?>
                        <label class="optiuni-text"><input type="checkbox" name="ingrediente[]" value="<?php echo $ingredient['id'];?>" class="optiuni-text"> <?php echo $ingredient['nume']?></label>
                      <?php endif;?>
                      
                    <?php endforeach;?>
                  </p>
                </div>
              </div>
              <p class="form-row form-row-wide">
                <label class="optiuni-text"><input type="checkbox" name="evidentiat" <?= isset($_POST['evidentiat']) && $_POST['evidentiat'] == 1 ? "checked" : (isset($item['evidentiat']) && $item['evidentiat'] == 1 ? "checked" : "") ?> value="1" class="optiuni-text"> Evidentiat pe prima pagina</label>
              </p>
              <p class="form-row">
                 <input type="submit" class="button" value="Salveaza" />
              </p>
           </form>
         </div>  
      </div>
   </div>
</div>
               