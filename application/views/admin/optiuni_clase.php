<div id="content" class="site-content" tabindex="-1" >
   <div class="col-full" style="margin-top: 20px;">
      <div class="row">
         <div class="col-md-3">
            <?php $this->load->view('admin/sidebar'); ?>
         </div>   
         <div class="col-md-9">
            <a href="<?= site_url('admin/optiuni_clase/adauga') ?>" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Adauga o clasa de optiuni</a><br><br>
            <?php if( isset( $success ) ): ?>
               <div class="alert alert-success alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
              </div>
            <?php endif; ?>
            <?php if(!empty($items)): ?>
               <table class="table table-bordered">
                  <thead>
                     <tr>
                       <th class="text-center">Optiune</th>
                       <th class="text-center">Editeaza/Sterge</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($items as $item): ?>
                      <tr>
                        <td><?= $item['nume'] ?></td>
                        <td class="text-center">
                            <a href="<?= site_url('admin/optiuni_clase/editeaza/'.$item['id']) ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            /
                            <a href="<?= site_url('admin/optiuni_clase/sterge/'.$item['id']) ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
               </table>
            <?php else: ?>
               <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> Nu exista clase de optiuni de afisat</div>
              </div>
            <?php endif; ?>

         </div>  
      </div>
   </div>
</div>
               