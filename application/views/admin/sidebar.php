<div id="nav_menu-2" class="widget widget_nav_menu mt-1">
   <div class="menu-food-menu-container">
      <ul id="menu-food-menu-2" class="menu">
         <li class="menu-item"><a href="<?= site_url("admin") ?>"><i class="po po-pizza"></i>Produse</a></li>
         <li class="menu-item"><a href="<?= site_url("admin/ingrediente") ?>"><i class="po po-pizza"></i>Ingrediente</a></li>
         <li class="menu-item"><a href="<?= site_url("admin/categorii") ?>"><i class="po po-fries"></i>Categorii</a></li>
         <li class="menu-item"><a href="<?= site_url("admin/optiuni_clase") ?>"><i class="po po-tacos"></i>Optiuni produse</a></li>
         <li class="menu-item"><a href="<?= site_url("admin/comenzi") ?>"><i class="po po-burger"></i>Comenzi</a></li>
         <!-- <li class="menu-item"><a href="shop-grid-3-column.html"><i class="po po-salads"></i>Salads</a></li>
         <li class="menu-item"><a href="shop-grid-3-column.html"><i class="po po-wraps"></i>Wraps</a></li>
         <li class="menu-item"><a href="shop-grid-3-column.html"><i class="po po-burger"></i>Fries</a></li>
         <li class="menu-item"><a href="shop-grid-3-column.html"><i class="po po-salads"></i>Salads</a></li>
         <li class="menu-item"><a href="shop-grid-3-column.html"><i class="po po-drinks"></i>Drinks</a></li> -->
      </ul>
   </div>
</div>