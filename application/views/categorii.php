<div id="content" class="site-content" tabindex="-1">
  <div class="col-full"><br>
    <h2><?= $categorie['nume'] ?></h2>
    <?php if( isset( $success ) ): ?>
       <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
      </div>
    <?php endif; ?>

    <?php if( isset( $error ) ): ?>
       <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
      </div>
    <?php endif; ?>

    <?php if( validation_errors()!="" ): ?>
       <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= validation_errors() ?></div>
      </div>
    <?php endif; ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" >
          <?php if(isset($produse) && !empty($produse)): ?>
           <div class="columns-3">
              <ul class="products">
                <?php $i = 0; for ($i=0; $i<count($produse); $i++): ?>
                <li class="product">
                    <div class="product-outer">
                       <div class="product-inner">
                          <div class="product-image-wrapper">
                             <a href="#" class="woocommerce-LoopProduct-link">
                             <img src="<?= base_url().'application/views/images/produse/'.$produse[$i]['image'] ?>" class="img-responsive" alt="">
                             </a>
                          </div>
                          <div class="product-content-wrapper">
                            <h3><?= $produse[$i]['nume'] ?></h3>
                            <form method="post" action="<?=site_url('cos/adauga')?>">
                              <input type="hidden" name="id_produs" value="<?= $produse[$i]['id'] ?>">
                              <?php for($j=0;$j<count($produse[$i]['optiuni']);$j=$j+1):?>
                              
                                <?php if(count($produse[$i]['optiuni'][$j]['optiuni']) > 0):?>

                              <div  class="yith_wapo_groups_container">
                                 <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                    <h3><span><?= $produse[$i]['optiuni'][$j]['nume'] ?></span></h3>
                                      <div class="row">
                                        <div class="col-sm-6">
                                          <fieldset>
                                          <?php foreach($produse[$i]['optiuni'][$j]['optiuni'] as $optiune):?>

                                            <label class="optiuni-text"><input type="radio" name="optiuni[<?= $produse[$i]['optiuni'][$j]['id'] ?>]" required value="<?=$optiune['id']?>" class="optiuni-text"> <?= $optiune['nume'] ?> <strong><?= $optiune['pret'] ?> lei</strong> </label>

                                          <?php endforeach;?>
                                          </fieldset>
                                          
                                        </div>
                                      </div>

                                 </div>
                              </div>
                            <?php endif;?>
                            <?php endfor;?>
                              <div class="hover-area">
<<<<<<< HEAD
                                <button class="button product_type_simple add_to_cart_button ajax_add_to_cart" >Adauga in cos</button>
=======
                                <div class="row">
                                        <div class="col-sm-6">
                                          <?php foreach($produse[$i]['ingrediente'] as $ingredient): ?>
                                             <label class="optiuni-text"><input type="checkbox" name="ingrediente_extra[]" checked="checked" value="<?= $ingredient['id'] ?>"> <?= $ingredient['nume'] ?></label>
                                          <?php endforeach; ?>
                                        </div>
                                        <div class="col-sm-6">
                                          <?php foreach($ingrediente as $ingredient): ?>
                                             <label class="optiuni-text"><input type="checkbox" name="ingrediente_extra[]" value="<?= $ingredient['id'] ?>"> Extra <?= $ingredient['nume'] ?></label>
                                          <?php endforeach; ?>
                                        </div> 
                                </div>
                                <button class="button product_type_simple add_to_cart_button ajax_add_to_cart">Adauga in cos</button>
>>>>>>> master
                              </div>
                            </form>
                          </div>
                       </div>
                    </div>
                    <!-- /.product-outer -->
                </li>
                
                <?php endfor; ?>
              </ul>
           </div>
          <?php else: ?>
            <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <div class="col-sm-offset-1"><i class="fa fa-times"></i> Nu exista produse de afisat.</div>
              </div>
          <?php endif; ?>
           <!-- <nav class="woocommerce-pagination">
              <ul class="page-numbers">
                 <li><span class="page-numbers current">1</span></li>
                 <li><a class="page-numbers" href="#">2</a></li>
                 <li><a class="page-numbers" href="#">3</a></li>
                 <li><a class="next page-numbers" href="#">Next Page &nbsp;&nbsp;&nbsp;→</a></li>
              </ul>
           </nav> -->
        </main>
        <!-- #main -->
     </div>
     <!-- #primary -->
  </div>
  <!-- .col-full -->
</div>

