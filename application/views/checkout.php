<div id="content" class="site-content" tabindex="-1" >
            <div class="col-full">
              <br>
               <div id="primary" class="content-area">
                  <main id="main" class="site-main" >
                    <div class="pizzaro-order-steps">
                      <ul>
                        <li class="cart">
                          <span class="step active-step">1</span><strong>Cos de cumparaturi</strong>
                        </li>
                        <li class="checkout">
                          <span class="step active-step">2</span><strong>Checkout</strong>
                        </li>
                        <li class="complete">
                          <span class="step">3</span>Comanda trimisa
                        </li>
                      </ul>
                    </div>
                    <?php if( isset( $success ) ): ?>
                       <div class="alert alert-success alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="row"><div class="col-sm-offset-1 cols-sm-11"><i class="fa fa-check"></i> <?=$success?></div></div> 
                      </div>
                    <?php endif; ?>

                    <?php if( isset( $error ) ): ?>
                       <div class="alert alert-danger alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
                      </div>
                    <?php endif; ?>

                    <?php if( validation_errors()!="" ): ?>
                       <div class="alert alert-danger alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= validation_errors() ?></div>
                      </div>
                    <?php endif; ?>

                    <div id="post-9" class="post-9 page type-page status-publish hentry">
                        <div class="entry-content">
                           <div class="woocommerce">
                              <form method="post" class="checkout woocommerce-checkout" action="<?= site_url('cos/trimite_comanda') ?>" enctype="multipart/form-data">
                                 <div class="col2-set" id="customer_details">
                                    <!-- <div class="col-1">
                                       <div class="woocommerce-billing-fields">
                                          <h3>Billing Details</h3>
                                          <p class="form-row form-row form-row-first validate-required" id="billing_first_name_field">
                                             <label for="billing_first_name" class="">First Name </label>
                                             <input type="text" class="input-text " name="billing_first_name" id="billing_first_name" placeholder=""  autocomplete="given-name" value=""  />
                                          </p>
                                          <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                             <label for="billing_last_name" class="">Last Name </label>
                                             <input type="text" class="input-text " name="billing_last_name" id="billing_last_name" placeholder=""  autocomplete="family-name" value=""  />
                                          </p>
                                          <div class="clear"></div>
                                          <p class="form-row form-row form-row-wide" id="billing_company_field">
                                             <label for="billing_company" class="">Company Name</label>
                                             <input type="text" class="input-text " name="billing_company" id="billing_company" placeholder=""  autocomplete="organization" value=""  />
                                          </p>
                                          <p class="form-row form-row form-row-first validate-required validate-email" id="billing_email_field">
                                             <label for="billing_email" class="">Email Address </label>
                                             <input type="email" class="input-text " name="billing_email" id="billing_email" placeholder=""  autocomplete="email" value=""  />
                                          </p>
                                          <p class="form-row form-row form-row-last validate-required validate-phone" id="billing_phone_field">
                                             <label for="billing_phone" class="">Phone </label>
                                             <input type="tel" class="input-text " name="billing_phone" id="billing_phone" placeholder=""  autocomplete="tel" value=""  />
                                          </p>
                                          <div class="clear"></div>
                                          <p class="form-row form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_country_field">
                                             <label for="billing_country" class="">Country </label>
                                             <input type="text" value="" placeholder="" id="billing_country" name="billing_phone" class="input-text ">
                                          </p>
                                          <div class="clear"></div>
                                          <p class="form-row form-row form-row-wide address-field validate-required" id="billing_address_1_field">
                                             <label for="billing_address_1" class="">Address </label>
                                             <input type="text" class="input-text " name="billing_address_1" id="billing_address_1" placeholder="Street address"  autocomplete="address-line1" value=""  />
                                          </p>
                                          <p class="form-row form-row form-row-wide address-field" id="billing_address_2_field">
                                             <input type="text" class="input-text " name="billing_address_2" id="billing_address_2" placeholder="Apartment, suite, unit etc. (optional)"  autocomplete="address-line2" value=""  />
                                          </p>
                                          <p class="form-row form-row form-row-wide address-field validate-required" id="billing_city_field">
                                             <label for="billing_city" class="">Town / City </label>
                                             <input type="text" class="input-text " name="billing_city" id="billing_city" placeholder=""  autocomplete="address-level2" value=""  />
                                          </p>
                                          <p class="form-row form-row form-row-first address-field validate-required validate-state" id="billing_state_field">
                                             <label for="billing_state" class="">State / County </label>
                                             <input type="text" value="" placeholder="" id="billing_state" name="billing_phone" class="input-text ">
                                          </p>
                                          <p class="form-row form-row form-row-last address-field validate-required validate-postcode" id="billing_postcode_field">
                                             <label for="billing_postcode" class="">Postcode / ZIP </label>
                                             <input type="text" class="input-text " name="billing_postcode" id="billing_postcode" placeholder=""  autocomplete="postal-code" value="DFSAF@GMAIL.COM"  />
                                          </p>
                                          <div class="clear"></div>
                                          <p class="form-row form-row-wide create-account">
                                             <input class="input-checkbox" id="createaccount"  type="checkbox" name="createaccount" value="1" />
                                             <label for="createaccount" class="checkbox">Create an account?</label>
                                          </p>
                                          <div class="create-account">
                                             <p>Create an account by entering the information below. If you are a returning customer please login at the top of the page.</p>
                                             <p class="form-row form-row validate-required" id="account_password_field">
                                                <label for="account_password" class="">Account password </label>
                                                <input type="password" class="input-text " name="account_password" id="account_password" placeholder="Password"   value=""  />
                                             </p>
                                             <div class="clear"></div>
                                          </div>
                                       </div>
                                    </div> -->
                                    <div class="col-2">
                                       <div class="woocommerce-shipping-fields">
                                          <h3>Informatii suplimentare</h3>
                                          <p> <strong>Noi livram la adresa:</strong>
                                          <?= $this->session->userdata('login')['oras'].", ".$this->session->userdata('login')['adr'] ?></p>
                                          <p class="form-row form-row form-row-wide address-field validate-required" id="billing_address_1_field">
                                             <label class="">Trimiteti la alta adresa:</label>
                                             <input type="text" class="input-text" name="adr" value="">
                                          </p>
                                          <p class="form-row form-row notes" id="order_comments_field">
                                             <label for="order_comments" class="">Alte detalii:</label>
                                             <textarea name="detalii" class="input-text" rows="3" cols="5" style="resize: none;"></textarea>
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <h3 id="order_review_heading">Comanda dumneavoastra</h3>
                                 <div id="order_review" class="woocommerce-checkout-review-order">
                                    <table class="shop_table woocommerce-checkout-review-order-table"  style="margin-bottom: 10px;">
                                       <thead>
                                          <tr>
                                             <th class="product-name">Produs</th>
                                             <th class="product-total">Total</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php foreach ($this->session->userdata('cos') as $produs): ?>
                                          <tr class="cart_item">
                                             <td class="product-name">
                                                <?= $produs['nume'] ?>&nbsp;<strong class="product-quantity">&times; <?= $produs['cantitate'] ?></strong>
                                                <dl class="variation">
                                                   <dt class="variation-PickSizespanclasswoocommerce-Price-amountamountspanclasswoocommerce-Price-currencySymbol36span2590span">Marime:
                                                   </dt>
                                                   <dd class="variation-PickSizespanclasswoocommerce-Price-amountamountspanclasswoocommerce-Price-currencySymbol36span2590span">
                                                      <p><?= $produs['marime'] ?></p>
                                                   </dd>
                                                </dl>
                                             </td>
                                             <td class="product-total">
                                                <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol"></span><?= $produs['pret']*$produs['cantitate'] ?> lei</span>
                                             </td>
                                          </tr>
                                        <?php endforeach; ?>
                                       </tbody>
                                       <tfoot>
                                          <!-- <tr class="cart-subtotal">
                                             <th>Subtotal</th>
                                             <td>
                                                <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">&#36;</span>51.80</span>
                                             </td>
                                          </tr> -->
                                          <tr class="order-total">
                                             <th>Total</th>
                                             <td>
                                                <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span><?= $total_cos ?> lei</span></strong>
                                             </td>
                                          </tr>
                                       </tfoot>
                                    </table>
                                    <div id="payment" class="woocommerce-checkout-payment">
                                      <ul class="wc_payment_methods payment_methods methods">
                                          <li class="wc_payment_method payment_method_cheque">
                                             <input id="payment_method_cheque" type="radio" class="input-radio" name="plata" value="1">
                                             <label for="payment_method_cheque">Plata cash</label>
                                             <!-- <input type="radio" class="input-radio" name="payment_method" value="cheque"  data-order_button_text="" /> -->
                                          </li>
                                          <li class="wc_payment_method payment_method_cod">
                                             <input id="payment_method_cod" type="radio" class="input-radio" name="plata" value="2">
                                             <label for="payment_method_cod">Plata cu cardul</label>
                                          </li>
                                       </ul>
                                      <div class="form-row place-order" style="padding-top: 0px;">
                                          <p class="form-row terms wc-terms-and-conditions">
                                             <!-- <input type="checkbox" class="input-checkbox" name="terms"  id="terms" />
                                             <label for="terms" class="checkbox">I&rsquo;ve read and accept the <a href="terms-and-conditions.html" target="_blank">terms &amp; conditions</a> <span class="required">*</span></label> -->
                                          </p>
                                          <button class="button alt text-center">Trimite comanda</button>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                        <!-- .entry-content -->
                     </div>
                     <!-- .entry-content -->
                </main><!-- #main -->
               </div>
               <!-- #post-## -->

            </div>
            <!-- #primary -->
         </div>