<div id="content" class="site-content" tabindex="-1" >
            <div class="col-full">
              <br>
               <div id="primary" class="content-area">
                  <main id="main" class="site-main" >
                    <div class="pizzaro-order-steps">
                      <ul>
                        <li class="cart">
                          <span class="step active-step">1</span><strong>Cos de cumparaturi</strong>
                        </li>
                        <li class="checkout">
                          <span class="step active-step">2</span><strong>Checkout</strong>
                        </li>
                        <li class="complete">
                          <span class="step active-step">3</span><strong>Comanda trimisa</strong>
                        </li>
                      </ul>
                    </div>
                    <?php if( isset( $success ) ): ?>
                       <div class="alert alert-success alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
                      </div>
                    <?php endif; ?>

                    <?php if( isset( $error ) ): ?>
                       <div class="alert alert-danger alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
                      </div>
                    <?php endif; ?>

                    <?php if( validation_errors()!="" ): ?>
                       <div class="alert alert-danger alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= validation_errors() ?></div>
                      </div>
                    <?php endif; ?>
                     <!-- .entry-content -->
                </main><!-- #main -->
               </div>
               <!-- #post-## -->

            </div>
            <!-- #primary -->
         </div>