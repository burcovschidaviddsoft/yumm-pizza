<div id="content" class="site-content" tabindex="-1" >
            <div class="col-full">
              <br>
               <div id="primary" class="content-area">
                  <main id="main" class="site-main" >
                    <div class="pizzaro-order-steps">
                      <ul>
                        <li class="cart">
                          <span class="step active-step">1</span><strong>Cos de cumparaturi</strong>
                        </li>
                        <li class="checkout">
                          <span class="step">2</span>Checkout
                        </li>
                        <li class="complete">
                          <span class="step">3</span>Comanda trimisa
                        </li>
                      </ul>
                    </div>
                    <?php if( isset( $success ) ): ?>
                       <div class="alert alert-success alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
                      </div>
                    <?php endif; ?>

                    <?php if( isset( $error ) ): ?>
                       <div class="alert alert-danger alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
                      </div>
                    <?php endif; ?>

                    <?php if( validation_errors()!="" ): ?>
                       <div class="alert alert-danger alert-dismissable">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= validation_errors() ?></div>
                      </div>
                    <?php endif; ?>

                     <div id="post-8" class="post-8 page type-page status-publish hentry">
                        <div class="entry-content">
                          <?php if(!empty($produse)): ?>
                           <div class="woocommerce">
                              <form method="post" action="<?=site_url('cos/salveaza')?>">
                                 <table class="shop_table shop_table_responsive cart" >
                                    <thead>
                                       <tr>
                                          <th class="product-remove">&nbsp;</th>
                                          <th class="product-thumbnail">&nbsp;</th>
                                          <th class="product-name">Produs</th>
                                          <th class="product-price">Pret</th>
                                          <th class="product-quantity">Cantitate</th>
                                          <th class="product-subtotal">Total</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                      <?php $k = 0; foreach ($produse as $produs): ?>
                                       <tr class="cart_item">
                                          <td class="product-remove">
                                             <a href="<?=site_url('cos/sterge/'.$k)?>" class="remove" >&times;</a>
                                          </td>
                                          <td class="product-thumbnail">
                                             <a href="single-product-v1.html">
                                             <img width="180" height="180" src="<?= base_url().'application/views/images/produse/'.$produs['image'] ?>" alt=""/>
                                             </a>
                                          </td>
                                          <td class="product-name" data-title="Produs">
                                             <a href="single-product-v1.html"><?= $produs['nume'] ?></a>
                                             <dl class="variation">
                                                <!-- <dt class="variation-Baseprice">Base price:</dt>
                                                <dd class="variation-Baseprice">
                                                   <p><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#036;</span>0.00</span></p>
                                                </dd> -->
                                                <dd><strong>Marime:</strong> <?= $produs['marime'] ?></dd>
                                                <?php if(isset($produs['op'])): ?>
                                                 	<?php foreach ($produs['op'] as $optiune): ?>
                                                    	<dd><?= $optiune['nume'] ?></dd>
                                          			 <?php endforeach; ?>
                                                <?php endif; ?>
                                             </dl>
                                          </td>
                                          <td class="product-price" data-title="Price">
                                             <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span><?= $produs['pret'] ?> lei</span>
                                          </td>
                                          <td class="product-quantity" data-title="Cantitate">
                                             <div class="qty-btn">
                                                <label>Cantitate</label>
                                                <div class="quantity">
                                                   <input type="number" name="cantitate[]" value="<?= $produs['cantitate'] ?>" title="Cantitate" class="input-text qty text"/>
                                                </div>
                                             </div>
                                          </td>
                                          <td class="product-subtotal" data-title="Total">
                                             <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span><?= $produs['pret']*$produs['cantitate'] ?> lei</span>
                                          </td>
                                       </tr>
                                      <?php $k++; endforeach; ?>
                                      <tr>
                                          <td colspan="6" class="actions">
                                             <!-- <div class="coupon">
                                                <label for="coupon_code">Coupon:</label>
                                                <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code" />
                                                <input type="submit" class="button" name="apply_coupon" value="Apply Coupon" />
                                             </div> -->
                                             <input type="submit" class="button" value="Actualizeaza" />
                                             <div class="wc-proceed-to-checkout">
                                                <a href="<?=site_url('cos/checkout')?>" class="checkout-button button alt wc-forward">Checkout</a>
                                             </div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </form>
                              <div class="cart-collaterals">
                                 <div class="cart_totals ">
                                    <h2>Total cos</h2>
                                    <table  class="shop_table shop_table_responsive">
                                       <!-- <tr class="cart-subtotal">
                                          <th>Subtotal</th>
                                          <td data-title="Subtotal">
                                             <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>51.80</span>
                                          </td>
                                       </tr> -->
                                       <tr class="order-total">
                                          <th>Total</th>
                                          <td data-title="Total">
                                             <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span><?= $total ?> lei</span></strong>
                                          </td>
                                       </tr>
                                    </table>
                                    <div class="wc-proceed-to-checkout">
                                       <a href="checkout.html" class="checkout-button button alt wc-forward">Proceed to Checkout</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                         <?php else: ?>
                            <div class="alert alert-danger alert-dismissable">
                              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                              <div class="col-sm-offset-1"><i class="fa fa-times"></i> Nu sunt produse in cos</div>
                          </div>
                         <?php endif ?>
                        </div>
                     </div>
                     <!-- .entry-content -->
                </main><!-- #main -->
               </div>
               <!-- #post-## -->

            </div>
            <!-- #primary -->
         </div>