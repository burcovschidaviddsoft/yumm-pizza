<footer id="colophon" class="site-footer footer-v1" >
   <div class="col-full">
      <div class="footer-social-icons">
         <span class="social-icon-text">Urmareste-ne</span>
         <ul class="social-icons list-unstyled">
            <li><a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/emanuelspizza/"></a></li>
            <li><a class="fa fa-twitter" href="#"></a></li>
            <li><a class="fa fa-instagram" href="#"></a></li>
            <li><a class="fa fa-youtube" href="#"></a></li>
            <li><a class="fa fa-dribbble" href="#"></a></li>
         </ul>
      </div>
      <div class="site-address">
         <ul class="address">
            <li>SC Yumm Pizza SRL</li>
            <li>Calea Unirii 22 Suceava</li>
            <li>Telefon: 0736 268 357</li>
         </ul>
      </div>
      <div class="site-info">
         <p class="copyright">Copyright &copy; <?= date('Y') ?> YummPizza. All rights reserved.</p>
      </div>
   </div>
   <!-- .col-full -->
</footer>
<script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/js/tether.min.js"></script>
<script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/js/owl.carousel.min.js"></script>

<script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/js/social.share.min.js"></script>
<script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/js/scripts.min.js"></script>
<!-- CKEDITOR -->
<script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/ckeditor/ckeditor.js"></script>
</body>
</html>