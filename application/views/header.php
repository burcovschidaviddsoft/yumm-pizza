<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>YummPizza.ro</title>
       <script src="//code.jquery.com/jquery-1.12.4.js"></script>
       <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
       <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
       <script type="text/javascript" src="<?= base_url().'application/views/' ?>assets/js/scripts.js"></script>
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
       <script src="<?= base_url().'application/views/' ?>assets/js/scripts.js"></script>
      <link rel="stylesheet" type="text/css" href="<?= base_url().'application/views/' ?>assets/css/bootstrap.min.css" media="all" />
      <link rel="stylesheet" type="text/css" href="<?= base_url().'application/views/' ?>assets/css/font-awesome.min.css" media="all" />
      <link rel="stylesheet" type="text/css" href="<?= base_url().'application/views/' ?>assets/css/animate.min.css" media="all" />
      <link rel="stylesheet" type="text/css" href="<?= base_url().'application/views/' ?>assets/css/font-pizzaro.css" media="all" />
       <link rel="stylesheet" type="text/css" href="<?= base_url().'application/views/' ?>assets/css/css.css" media="all" />
      <link rel="stylesheet" type="text/css" href="<?= base_url().'application/views/' ?>assets/css/style.css" media="all" />
      <link rel="stylesheet" type="text/css" href="<?= base_url().'application/views/' ?>assets/css/colors/red.css" media="all" />
      <link rel="stylesheet" type="text/css" href="<?= base_url().'application/views/' ?>assets/css/jquery.mCustomScrollbar.min.css" media="all" />
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CYanone+Kaffeesatz:200,300,400,700" rel="stylesheet">
      <link rel="shortcut icon" href="<?= base_url().'application/views/' ?>/images/logo_yumm.png">
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
       <!--
      <script type="text/javascript">
      	//$(document).ready(function() {
	      	$(window).scroll(function() {
		      if ($(this).scrollTop() > 1){
		      $('.site-header').addClass('sticky-header');
		      $('.logo').addClass('sticky-logo');
		      }
		      else{
		      $('.site-header').removeClass('sticky-header');
		      $('.logo').removeClass('sticky-logo');
		     }
		    });
      </script>
      -->
   </head>
   <body class="page-template-template-homepage-v1 home-v1">
      <div id="page" class="hfeed site">
         <header id="masthead" class="site-header header-v1" style="padding-top:0px !important;z-index:1000">
             <div class="acesta_este_sidebarul" id="scrollbar_l">
                 <p class="meniuri_l">Meniuri</p>
                 <p class="meniu_link_l" id="1">Categorie 1</p>
                 <p class="meniu_link_l">Categorie 2</p>
                 <p class="meniu_link_l">Categorie 3</p>
                 <p class="meniu_link_l">Categorie 4</p>
                 <p class="meniu_link_l">Categorie 5</p>
                 <p class="meniu_link_l">Categorie 6</p>
                 <p class="meniu_link_l">Categorie 7</p>
                 <i class="fa fa-cart-arrow-down iconita_sidebar_l" aria-hidden="true"></i>
                 <i class="fa fa-phone iconita_sidebar_l" id="show_phone" aria-hidden="true"></i>
                 <p class="meniu_link_l" id="telefon_l">0248 000 000</p>
             </div>
             <div class="navigator_l">
                 <div class="site-branding">
                     <a href="<?= base_url() ?>" class="custom-logo-link" rel="home">
                         <img  src="<?= base_url().'application/views/' ?>/images/logo_yumm.png" class="logo_l" alt="">
                     </a>
                 </div>
                 <div class="primary-navigation">
                     <ul id="menu-main-menu" class="menu nav-menu" aria-expanded="false">
                         <li class="menu-item"><a href="<?=site_url()?>">ACASA</a></li>
                         <li class="menu-item"><a href="<?=site_url('despre-noi')?>">DESPRE NOI</a></li>
                         <li class="menu-item"><a href="<?=site_url('contact')?>">CONTACT</a></li>
                     </ul>
                 </div>
                 <i class="fa fa-bars open_side_l" id="open_sidebar_l" aria-hidden="true"></i>
                 <i class="fa fa-times open_side_l" id="close_sidebar_l" aria-hidden="true"></i>

             </div>
             <!--
=======
          <div class="acesta_este_sidebarul" id="scrollbar_l">
              <p class="meniuri_l">Meniuri</p>
              <?php foreach($categorii as $categorie): ?>
                <a href="/meniu/<?= $categorie['slug'] ?>"><p class="meniu_link_l"><?= $categorie['nume'] ?></p></a>  
              <?php endforeach;?>
              <i class="fa fa-cart-arrow-down iconita_sidebar_l" aria-hidden="true"></i>
              <i class="fa fa-phone iconita_sidebar_l" aria-hidden="true"></i>
          </div>
          <div class="navigator_l">
              <div class="site-branding">
                  <a href="<?= base_url() ?>" class="custom-logo-link" rel="home">
                      <img  src="<?= base_url().'application/views/' ?>/images/logo_yumm.png" class="logo_l" alt="">
                  </a>
              </div>
              <div class="primary-navigation">
              <ul id="menu-main-menu" class="menu nav-menu" aria-expanded="false">
                  <li class="menu-item"><a href="<?=site_url()?>">ACASA</a></li>
                  <li class="menu-item"><a href="<?=site_url('despre-noi')?>">DESPRE NOI</a></li>
                  <li class="menu-item"><a href="<?=site_url('contact')?>">CONTACT</a></li>
              </ul>
              </div>
              <i class="fa fa-bars open_side_l" id="open_sidebar_l" aria-hidden="true"></i>
              <i class="fa fa-times open_side_l" id="close_sidebar_l" aria-hidden="true"></i>
          </div>

          <!--
         <header id="masthead" class="site-header header-v1" style="background-image: none;">
=======
         <header id="masthead" class="site-header header-v1">
>>>>>>> master
>>>>>>> master
            <div class="col-full">
               <div class="header-wrap">
                  <div class="site-branding" style="margin: auto;">
                     <a href="<?= base_url() ?>" class="custom-logo-link" rel="home">
                        <img width="150" src="<?= base_url().'application/views/' ?>/images/logo_yumm.png" class="img-responsive logo" alt="">
                     </a>
                  </div>
<<<<<<< HEAD
                  <nav id="site-navigation" class="main-navigation"  aria-label="Primary Navigation">
                     <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span class="close-icon"><i class="po po-close-delete"></i></span><span class="menu-icon"><i class="po po-menu-icon"></i></span><span class=" screen-reader-text">Menu</span></button>
                     <div class="primary-navigation">
                        <ul id="menu-main-menu" class="menu nav-menu" aria-expanded="false">
                           <li class="menu-item"><a href="<?=site_url()?>">ACASA</a></li>
                           <li class="yamm-fw menu-item menu-item-has-children">
                              <a>MENIU</a>
                              <ul class="sub-menu">
                                 <?php foreach ($categorii as $categorie): ?>
                                    <li class="nav-title menu-item"><a href="<?=site_url('meniu/'.$categorie['slug'])?>"><?= $categorie['nume'] ?></a></li>
                                 <?php endforeach; ?>
                              </ul>
                           </li>
                           <li class="menu-item"><a href="<?=site_url('despre-noi')?>">DESPRE NOI</a></li>
                           <li class="menu-item"><a href="<?=site_url('contact')?>">CONTACT</a></li>
                        </ul>
                     </div>
                     <div class="handheld-navigation">
                        <span class="phm-close">Inchide</span>
                        <ul  class="menu">
                           <li class="menu-item "><a href="<?=site_url()?>"><i class="po po-pizza"></i> Acasa</a></li>
                           <?php foreach ($categorii as $categorie): ?>
                              <li class="menu-item "><a href="<?=site_url($categorie['slug'])?>"><i class="po po-pizza"></i> <?= $categorie['nume'] ?></a></li>
                           <?php endforeach; ?>
                           <li class="menu-item "><a href="<?=site_url('despre-noi')?>"><i class="po po-pizza"></i> Despre noi</a></li>
                           <li class="menu-item "><a href="<?=site_url('contact')?>"><i class="po po-pizza"></i> Contact</a></li>
                           <!-- <li class="menu-item "><a href="shop-grid-3-column.html"><i class="po po-burger"></i>Burgers</a></li>
                           <li class="menu-item "><a href="shop-grid-3-column.html"><i class="po po-salads"></i>Salads</a></li>
                           <li class="menu-item "><a href="shop-grid-3-column.html"><i class="po po-tacos"></i>Tacos</a></li>
                           <li class="menu-item "><a href="shop-grid-3-column.html"><i class="po po-wraps"></i>Wraps</a></li>
                           <li class="menu-item "><a href="shop-grid-3-column.html"><i class="po po-fries"></i>Fries</a></li>
                           <li class="menu-item "><a href="shop-grid-3-column.html"><i class="po po-salads"></i>Salads</a></li>
                           <li class="menu-item "><a href="shop-grid-3-column.html"><i class="po po-drinks"></i>Drinks</a></li> -->
          <!--
                        </ul>
                     </div>
                  </nav>
      -->
                  <!-- #site-navigation -->
          <!--
                  <div class="header-info-wrapper">
                     <div class="header-phone-numbers">
                        <span class="intro-text">
                           <?php if($this->simpleloginsecure->is_logat()): ?>
                              <a class="intro-text" href="#"> Contul meu </a> / <a class="intro-text" href="<?= $this->simpleloginsecure->is_admin() ? site_url('admin/login/logout') : site_url('login/logout') ?>">Logout</a>
                           <?php else: ?> 
                              <a class="intro-text" href="<?= site_url('login/inregistrare') ?>"> Cont nou </a>
                              /
                              <a class="intro-text" href="<?= site_url('login') ?>"> Login </a> 
                           <?php endif; ?>
                        </span>
                        <span id="city-phone-number-label" class="phone-number">0736 268 357</span>
                     </div>
                     <ul class="site-header-cart-v2 menu">
                        <li class="cart-content ">
                           <a href="<?= site_url('cos') ?>" title="View your shopping cart">
                           <i class="po po-scooter"></i>
                           <span>Produsele mele</span>
                           </a>
                           <ul class="sub-menu">
                              <li>
                                 <a href="<?= site_url('cos') ?>" title="View your shopping cart">
                                 <span class="count"><?= $items_cos ?> produse</span> <span class="amount"><?= $total_cos ?> lei</span>
                                 </a>
                              </li>
                           </ul>
                        </li>
                     </ul>
                  </div>
=======
>>>>>>> master
               </div>
            </div>
            -->
         </header>
