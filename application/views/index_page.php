<div id="content" class="site-content" tabindex="-1" >
            <div class="col-full">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main" >
                     <div class="tiles">
                        <div class="row">
                           <div class="col-xs-12 col-sm-6">
                              <div class="banner top-left" >
                                 <a href="#">
                                    <div class="banner-bg" style="background-size: cover; background-position: center center; background-image: url( <?= base_url().'application/views/' ?>assets/images/homepage-banners/h1.jpg ); height: 442px;">
                                       <!-- <div class="caption">
                                          <h3 class="title">GRILLED CHICKEN</h3>
                                          <h4 class="subtitle">SUMMER PIZZA</h4>
                                          <span class="button">HOT &amp; SPICY</span>
                                          <span class="condition"><em>*</em>ONLY IN LOCAL</span>
                                       </div> -->
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-6">
                              <div class="banners">
                                 <div class="row">
                                    <div class="banner col-sm-6 top-right" >
                                       <a href="#">
                                          <div class="banner-bg" style="background-size: cover; background-position: center center; background-image: url( <?= base_url().'application/views/' ?>assets/images/homepage-banners/h2.jpg ); height: 210px;">
                                             <!-- <div class="caption">
                                                <h3 class="title">FREE</h3>
                                                <h4 class="subtitle">FRIES</h4>
                                                <div class="description">for online orders in wendsdays</div>
                                             </div> -->
                                          </div>
                                       </a>
                                    </div>
                                    <div class="banner col-sm-6 top-right" >
                                       <a href="#">
                                          <div class="banner-bg" style="background-size: cover; background-position: center center; background-image: url( <?= base_url().'application/views/' ?>assets/images/homepage-banners/h3.jpg ); height: 210px;">
                                             <!-- <div class="caption">
                                                <h3 class="title">iCED COFFEE</h3>
                                                <h4 class="subtitle">SUMMERS</h4>
                                                <span class="condition"><em>*</em>ONLY IN LOCAL</span>
                                             </div> -->
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="banner center" >
                                 <a href="#">
                                    <div class="banner-bg" style="background-size: cover; background-position: center center; background-image: url( <?= base_url().'application/views/' ?>assets/images/homepage-banners/h4.jpg  ); height: 210px;">
                                       <!-- <div class="caption">
                                          <h3 class="title"><span>ORDER</span> ONLINE</h3>
                                       </div> -->
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="section-tabs">
                        <ul class="nav nav-tabs pizzaro-nav-tabs" >
                           <li class="nav-item">
                              <a href="#h1-tab-products-1" data-toggle="tab">Wraps</a>
                           </li>
                           <li class="nav-item active">
                              <a href="#h1-tab-products-2" class="active" data-toggle="tab">Pizza Sets</a>
                           </li>
                           <li class="nav-item">
                              <a href="#h1-tab-products-3" data-toggle="tab">Burgers</a>
                           </li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane" id="h1-tab-products-1">
                              <div class="section-products">
                                 <div class="woocommerce columns-3">
                                    <div class="columns-3">
                                       <ul class="products">
                                          <li class="product first">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p1.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Pepperoni Pizza</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="product ">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p2.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Trio Cheese</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="product last">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p3.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Apricot Chicken</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane active" id="h1-tab-products-2">
                              <div class="section-products">
                                 <div class="woocommerce columns-3">
                                    <div class="columns-3">
                                       <ul class="products">
                                          <li class="product first">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p4.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Italiano Original</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="product ">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p5.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Chicken Hawaii</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="product last">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p6.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Summer Pizza</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="h1-tab-products-3">
                              <div class="section-products">
                                 <div class="woocommerce columns-3">
                                    <div class="columns-3">
                                       <ul class="products">
                                          <li class="product first">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p7.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Pepperoni Calzone</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="product ">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p8.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Pepperoni Pizza</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="product last">
                                             <div class="product-outer">
                                                <div class="product-inner">
                                                   <div class="product-image-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                      <img src="<?= base_url().'application/views/' ?>assets/images/products/p9.jpg" class="img-responsive" alt="">
                                                      </a>
                                                   </div>
                                                   <div class="product-content-wrapper">
                                                      <a href="single-product-v1.html" class="woocommerce-LoopProduct-link">
                                                         <h3>Trio Cheese</h3>
                                                         <div itemprop="description">
                                                            <p style="max-height: none;">Extra-virgin olive oil, garlic, mozzarella, mushrooms and olives.</p>
                                                         </div>
                                                         <div  class="yith_wapo_groups_container">
                                                            <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                               <h3><span>Pick Size</span></h3>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">22 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>19</span></span>
                                                               </div>
                                                               <div class="ywapo_input_container ywapo_input_container_radio">

                                                                  <span class="ywapo_label_tag_position_after"><span class="ywapo_option_label ywapo_label_position_after">29 cm</span></span>
                                                                  <span class="ywapo_label_price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>25</span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </a>
                                                      <div class="hover-area">
                                                         <a rel="nofollow" href="single-product-v1.html" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div> -->
                    <?php if(!empty($produse)): ?>
                     <div class="section-products">
                        <h2 class="section-title">Produse speciale</h2>
                        <div class="columns-3">
                           <ul class="products">
                           	<?php $i = 0; for ($i=0; $i<count($produse); $i++): ?>
				                <li class="product">
				                    <div class="product-outer">
				                       <div class="product-inner">
				                          <div class="product-image-wrapper">
				                             <a href="#" class="woocommerce-LoopProduct-link">
				                             <img src="<?= base_url().'application/views/images/produse/'.$produse[$i]['image'] ?>" class="img-responsive" alt="">
				                             </a>
				                          </div>
				                          <div class="product-content-wrapper">
				                            <h3><?= $produse[$i]['nume'] ?></h3>
				                            <form method="post" action="<?=site_url('cos/adauga')?>">
				                              <input type="hidden" name="id_produs" value="<?= $produse[$i]['id'] ?>">
				                              <div  class="yith_wapo_groups_container">
				                                 <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
				                                    <h3><span>Optiuni</span></h3>
				                                      <div class="row">
				                                        <div class="col-sm-6">
				                                          <p class="optiuni-titlu"><strong>Selecteaza marime:</strong></p>
				                                          <?= !empty($produse[$i]['mica']) && !empty($produse[$i]['pret_mica']) ? '<label class="optiuni-text"><input type="radio" name="marime" required value="1" class="optiuni-text"> '.$produse[$i]['mica'].' <strong>'.$produse[$i]['pret_mica'].' lei</strong> </label>' : "" ?>
				                                          <?= !empty($produse[$i]['medie']) && !empty($produse[$i]['pret_medie']) ? '<label class="optiuni-text"><input type="radio" name="marime" value="2" class="optiuni-text"> '.$produse[$i]['medie'].' <strong>'.$produse[$i]['pret_medie'].' lei</strong> </label>' : "" ?>
				                                          <?= !empty($produse[$i]['mare']) && !empty($produse[$i]['pret_mare']) ? '<label class="optiuni-text"><input type="radio" name="marime" value="3" class="optiuni-text"> '.$produse[$i]['mare'].' <strong>'.$produse[$i]['pret_mare'].' lei</strong> </label>' : "" ?>
				                                        </div>
				                                        <div class="col-sm-6">
				                                          <?php if( !empty($produse[$i]['optiuni']) ): ?>
				                                            <p class="optiuni-titlu"><strong>Suplimentar:</strong></p>
				                                            <?php foreach ($produse[$i]['optiuni'] as $optiune): ?>
				                                              <label class="optiuni-text"><input type="checkbox" name="optiuni[]" value="<?=$optiune['id']?>" class="optiuni-text"> <?=$optiune['nume']?></label>
				                                            <?php endforeach; ?>
				                                          <?php endif; ?>
				                                        </div>
				                                      </div>
				                                 </div>
				                              </div>
				                              <div class="hover-area">
				                                <button class="button product_type_simple add_to_cart_button ajax_add_to_cart">Adauga in cos</button>
				                              </div>
				                            </form>
				                          </div>
				                       </div>
				                    </div>
				                    <!-- /.product-outer -->
				                </li>
				            <?php endfor; ?>
			                <?php if($i%3 != -0): ?>
			                  <li class="product visible-lg" style="visibility: hidden;">
			                      <div class="product-outer">
			                         <div class="product-inner">
			                            <div class="product-image-wrapper">
			                               <a href="#" class="woocommerce-LoopProduct-link">
			                               <img src="<?= base_url().'application/views/' ?>images/produse/10.jpg" class="img-responsive" alt="">
			                               </a>
			                            </div>
			                            <div class="product-content-wrapper">
			                              <h3>PIZZA</h3>
			                              <div itemprop="description">
			                                 <p style="max-height: none;">desctiere...</p>
			                              </div>
			                              <form method="post">
			                                <div  class="yith_wapo_groups_container">
			                                   <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
			                                      <h3><span>Optiuni</span></h3>
			                                        <div class="row">
			                                          <div class="col-sm-6">
			                                            <p class="optiuni-text"><strong>Selecteaza marime:</strong></p>
			                                            <label class="optiuni-text"><input type="radio" name="marime" value="mare" class="optiuni-text"> 25 cm <strong>29.99 lei</strong> </label>
			                                            <label class="optiuni-text"><input type="radio" name="marime" value="mica" class="optiuni-text"> 32 cm <strong>30.59 lei</strong> </label>
			                                            <label class="optiuni-text"><input type="radio" name="marime" value="medie" class="optiuni-text"> 40 cm <strong>49.99 lei</strong> </label>
			                                          </div>
			                                          <div class="col-sm-6">
			                                            <p class="optiuni-text"><strong>Suplimentar:</strong></p>
			                                            <label class="optiuni-text"><input type="checkbox" name="" value="" class="optiuni-text"> Original Crust</label>
			                                            <label class="optiuni-text"><input type="checkbox" name="" value="" class="optiuni-text"> Original Crust</label>
			                                            <label class="optiuni-text"><input type="checkbox" name="" value="" class="optiuni-text"> Original Crust</label>
			                                          </div>
			                                        </div>
			                                   </div>
			                                </div>
			                                <div class="hover-area">
			                                  <a rel="nofollow" href="#" data-quantity="1" data-product_id="51" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Adauga in cos</a>
			                                </div>
			                              </form>
			                            </div>
			                         </div>
			                      </div>
			                      <!-- /.product-outer -->
			                  </li>
			                <?php endif; ?>
                           </ul>
                        </div>
                     </div>
                 	<?php endif; ?>
                     <div class="features-list" >
                        <div class="row">
                           <div class="feature col-xs-12 col-sm-3">
                              <div class="feature-icon"><i class="po po-best-quality"></i></div>
                              <div class="feature-label">
                                 <h4>Cea mai buna calitate</h4>
                                 <p>Lorem Ipsum este pur şi simplu o machetă pentru text a industriei tipografice.</p>
                              </div>
                           </div>
                           <div class="feature col-xs-12 col-sm-3">
                              <div class="feature-icon"><i class="po po-on-time"></i></div>
                              <div class="feature-label">
                                 <h4>Mereu la timp</h4>
                                 <p>Lorem Ipsum este pur şi simplu o machetă pentru text a industriei tipografice.</p>
                              </div>
                           </div>
                           <div class="feature col-xs-12 col-sm-3">
                              <div class="feature-icon"><i class="po po-master-chef"></i></div>
                              <div class="feature-label">
                                 <h4>Maestri bucatari</h4>
                                 <p>Lorem Ipsum este pur şi simplu o machetă pentru text a industriei tipografice.</p>
                              </div>
                           </div>
                           <div class="feature col-xs-12 col-sm-3">
                              <div class="feature-icon"><i class="po po-ready-delivery"></i></div>
                              <div class="feature-label">
                                 <h4>Mancare delicioasa</h4>
                                 <p>Lorem Ipsum este pur şi simplu o machetă pentru text a industriei tipografice.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- .col-full -->
         </div>