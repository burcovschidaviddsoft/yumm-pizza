<div id="content" class="site-content" tabindex="-1" >
   <div class="col-full" style="margin-top: 20px;">
      <div class="row">
         <div class="col-md-12">
            <div class="customer-login-form">
               <div class="u-columns col2-set">
                  <div class="u-column1 col-1">
                     <form method="post" action="<?= $link != '' ? site_url('login/salveaza?link='.$link) :  site_url('login/salveaza') ?>">
                        <!-- <p class="form-row form-row-wide">
                           <label for="user_email_login">Email address <span class="required">*</span></label>
                           <input type="text" class="input-text" required name="user_email_login" value="" />
                        </p>
                        <p class="form-row form-row-wide">
                           <label for="user_pass_login">Password <span class="required">*</span></label>
                           <input class="input-text" type="password" required name="user_pass_login" />
                        </p> -->
                        <div class="col2-set" id="customer_details">
                           <div class="col-1">
                              <div class="woocommerce-billing-fields">
                                 <h3>Inregistrare</h3>
                                 <?php if(isset($error)): ?>
                                    <div class="alert alert-danger alert-dismissable">
                                       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                       <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $error ?></div>
                                   </div>
                                 <?php endif; ?>
                                 <?php if( isset( $success ) ): ?>
                                    <div class="alert alert-success alert-dismissable">
                                       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                       <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
                                   </div>
                                 <?php endif; ?>
                                 <p class="form-row form-row form-row-first">
                                    <label for="billing_first_name" class="">Nume <span class="required">*</span></label>
                                    <input type="text" required class="input-text" name="nume" value="<?=isset( $_POST['nume'] ) ? $_POST['nume'] : (isset($item['nume']) ? $item['nume'] : "")?>">
                                 </p>
                                 <p class="form-row form-row form-row-last">
                                    <label for="billing_last_name" class="">Telefon</label>
                                    <input type="text" class="input-text" name="tel" value="<?=isset( $_POST['tel'] ) ? $_POST['tel'] : (isset($item['tel']) ? $item['tel'] : "")?>">
                                 </p>
                                 <div class="clear"></div>
                                 <p class="form-row form-row form-row-first validate-email">
                                    <label for="billing_email" class="">Email Address <span class="required">*</span></label>
                                    <input type="email" required class="input-text" name="email" value="<?=isset( $_POST['email'] ) ? $_POST['email'] : (isset($item['email']) ? $item['email'] : "")?>">
                                 </p>
                                 <p class="form-row form-row form-row-last">
                                    <label for="billing_phone" class="">Parola <span class="required">*</span></label>
                                    <input type="password" required class="input-text" name="parola" value="<?=isset( $_POST['parola'] ) ? $_POST['parola'] : (isset($item['parola']) ? $item['parola'] : "")?>">
                                 </p>
                                 <div class="clear"></div>
                                 <p class="form-row form-row form-row-wide address-field">
                                    <label for="billing_city" class="">Oras <span class="required">*</span></label>
                                    <input type="text" required class="input-text" name="oras" value="<?=isset( $_POST['oras'] ) ? $_POST['oras'] : (isset($item['oras']) ? $item['oras'] : "")?>">
                                 </p>
                                 <p class="form-row form-row form-row-wide address-field">
                                    <label for="billing_address_1" class="">Addresa <span class="required">*</span></label>
                                    <input type="test" required class="input-text" name="adr" value="<?=isset( $_POST['adr'] ) ? $_POST['adr'] : (isset($item['adr']) ? $item['adr'] : "")?>">
                                 </p>
                                 <div class="clear"></div>
                                 <p class="form-row">
                                    <input type="submit" class="button" name="login" value="Inregistrare" />
                                 </p>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div> 
      </div>
   </div>
</div>
               