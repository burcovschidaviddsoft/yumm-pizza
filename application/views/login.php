<div id="content" class="site-content" tabindex="-1" >
   <div class="col-full" style="margin-top: 20px;">
      <div class="row">
         <div class="col-md-12">
            <div class="customer-login-form">
               <div class="u-columns col2-set">
                  <div class="u-column1 col-1">
                     <h2>Login</h2>
                     <form method="post" action="<?= $link != '' ? site_url('login/verificare?link='.$link) :  site_url('login/verificare') ?>">
                        <?php if(isset($errorLogin)): ?>
                           <div class="alert alert-danger alert-dismissable">
                              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                              <div class="col-sm-offset-1"><i class="fa fa-times"></i> <?= $errorLogin ?></div>
                          </div>
                        <?php endif; ?>
                        <?php if( isset( $success ) ): ?>
                           <div class="alert alert-success alert-dismissable">
                              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                              <div class="col-sm-offset-1"><i class="fa fa-check"></i> <?=$success?></div>
                          </div>
                        <?php endif; ?>
                        <p class="form-row form-row-wide">
                           <label for="user_email_login">Email <span class="required">*</span></label>
                           <input type="text" class="input-text" required name="user_email_login" value="" />
                        </p>
                        <p class="form-row form-row-wide">
                           <label for="user_pass_login">Parola <span class="required">*</span></label>
                           <input class="input-text" type="password" required name="user_pass_login" />
                        </p>
                        <p class="form-row">
                           <input type="submit" class="button" name="login" value="Login" />
                        </p>
                        <p class="lost_password">
                           <a href="<?= $link != '' ? site_url('login/inregistrare?link='.$link) :  site_url('login/inregistrare') ?>">Nu ai cont? Creeaza unul aici!</a>
                        </p>
                     </form>
                  </div>
               </div>
            </div>
         </div> 
      </div>
   </div>
</div>
               