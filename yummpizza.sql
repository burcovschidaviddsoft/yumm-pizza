-- MySQL dump 10.16  Distrib 10.1.28-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: yummpizza
-- ------------------------------------------------------
-- Server version	10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorii`
--

DROP TABLE IF EXISTS `categorii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorii`
--

LOCK TABLES `categorii` WRITE;
/*!40000 ALTER TABLE `categorii` DISABLE KEYS */;
INSERT INTO `categorii` VALUES (1,'Pizza','pizza'),(2,'test teste','test-teste'),(3,'Sucuri','sucuri');
/*!40000 ALTER TABLE `categorii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('0jlopmqmkon15heaqudhfcuml0460lmq','::1',1511282012,'__ci_last_regenerate|i:1511282012;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('0vgtchd9ug02cakc65rp8fust4dui3m8','::1',1511220426,'__ci_last_regenerate|i:1511220426;'),('1ec7qsn9tv96893r7pj4tuhqfinb0r57','::1',1511185531,'__ci_last_regenerate|i:1511185487;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('1n2t7i6uffhm1hercbdp4kv63o7q55u8','::1',1511173300,'__ci_last_regenerate|i:1511173300;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('2dhd63s4kmrk477ibj2g8406u0q97g79','::1',1511180391,'__ci_last_regenerate|i:1511180391;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('36dt5vuimnb9p4nl8okkkjc5l9847fr1','::1',1511083797,'__ci_last_regenerate|i:1511083797;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3c9dibbs8c93bk0o1p7iee4cufo3sb4b','::1',1511280490,'__ci_last_regenerate|i:1511280490;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3cee8bgd7vsf97c9esalrg936i05cm1b','::1',1510836119,'__ci_last_regenerate|i:1510836119;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3e0eg1aseprd220tra97jd3gof3hgq69','::1',1510753097,'__ci_last_regenerate|i:1510753097;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3et3u054j6vt080qv4ur85a9md0k42at','::1',1511277508,'__ci_last_regenerate|i:1511277508;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3fagacr06mbb1bquejdh7hhrkd5pejhg','::1',1510749680,'__ci_last_regenerate|i:1510749680;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3i572p4djbt9ai7lhpmq8gtrdon87jnt','::1',1510736993,'__ci_last_regenerate|i:1510736993;'),('3qfc14mlq9mmfi0f1j3o7978akekgc5f','::1',1510737365,'__ci_last_regenerate|i:1510737365;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3th92dhlq94a7sje0r0kju604atjnjbo','::1',1511281669,'__ci_last_regenerate|i:1511281669;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3uk859kijh0hc98s2v40e8cd5tichlo0','::1',1511051520,'__ci_last_regenerate|i:1511051517;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('4hgm6tqcdi02liu24pgrr95v1sm8pg0n','::1',1510752795,'__ci_last_regenerate|i:1510752795;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('5bulrtk0n8vce9d7mrno1i5rktp1i60d','::1',1511098431,'__ci_last_regenerate|i:1511098431;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('7bcoj01f6vo4lmqd65o1r8ch72m6e2lr','::1',1511264596,'__ci_last_regenerate|i:1511264596;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('7hlf862pglk53d6bc57kfpq183hue5u7','::1',1510751772,'__ci_last_regenerate|i:1510751772;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('7ov7d059rh7tgguq87n15743fiehe4g7','::1',1511117934,'__ci_last_regenerate|i:1511117930;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('9atfidcjc7l0nd3149i1v2n4ch00lmm8','::1',1511221565,'__ci_last_regenerate|i:1511221565;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('9b5s6m09nbjsarl4pv35s9kek7ktk5hq','::1',1511185487,'__ci_last_regenerate|i:1511185487;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('9f7a3tskiavh8535e2lfpftnoj6pel4k','::1',1511212847,'__ci_last_regenerate|i:1511212847;'),('9r8gqp3kbjc8prlhnfoegdtlg6gdni75','::1',1510752492,'__ci_last_regenerate|i:1510752492;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ad50r699nvv5c6jf9eqn229l1drijtan','::1',1511180070,'__ci_last_regenerate|i:1511180070;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('agi63vvdj3h1m37ajfpupc9u7bhk815t','::1',1511179753,'__ci_last_regenerate|i:1511179753;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('boqmk6baioa2h12aos8qic9ck7c62kvj','::1',1510741322,'__ci_last_regenerate|i:1510741281;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('c8bi5hc1c7d648iqf5kkla48445ptpvq','::1',1510740480,'__ci_last_regenerate|i:1510740480;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('dc47ubqjna7n3tt8499frb8ig2dpva5s','::1',1510751231,'__ci_last_regenerate|i:1510751231;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('e33q7631vjcof7pfm22ml5e63long6js','::1',1511217522,'__ci_last_regenerate|i:1511217522;'),('ep3esoknphttibghuneqm0plsicokeds','::1',1511221921,'__ci_last_regenerate|i:1511221921;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('f0244ibhn2k1nfpnocopcbagtjnudmlq','::1',1511096306,'__ci_last_regenerate|i:1511096306;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}success|s:33:\"Optiunea a fost stearsa cu succes\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('f8a8e8qv5cvn980rv2q1a1h5s2h10un3','::1',1511277819,'__ci_last_regenerate|i:1511277819;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('gbpe0p23kcb2amj1eth5s8earup4s8eq','::1',1511181402,'__ci_last_regenerate|i:1511181402;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('h0amibjq5nfnf6kipuip6bdamoav6hv5','::1',1511085512,'__ci_last_regenerate|i:1511085512;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('h15unrfgk6kk7h84d5de576gae97uucj','::1',1511085211,'__ci_last_regenerate|i:1511085211;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('h4brjq4vqgoa0e8obmblntvub434rkng','::1',1511181089,'__ci_last_regenerate|i:1511181089;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('hg78aqnasubccgp80dh3i9jj90q9glvi','::1',1511085546,'__ci_last_regenerate|i:1511085512;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('hkt86bjnpqovlrckmc4m368gu3gjhsrb','::1',1511267537,'__ci_last_regenerate|i:1511267537;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ik50btui77bnqocj1nifroqj93icugp1','::1',1511098431,'__ci_last_regenerate|i:1511098431;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ik7lg9irjv97f3ak2se3pnem2t1974kq','::1',1511084125,'__ci_last_regenerate|i:1511084125;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ini49om2qlcbvalmbbccg6hnod7ik543','::1',1510750800,'__ci_last_regenerate|i:1510750800;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('it3kf1k8hv074tn8fsgfb2hjj6iqab4j','::1',1511268258,'__ci_last_regenerate|i:1511268258;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('j17juk1m6ofejm6cqpuog4heioql0ceq','::1',1511117930,'__ci_last_regenerate|i:1511117930;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('kdjqpsoikf2oshus5nn0gidvv2rghtdg','::1',1511276875,'__ci_last_regenerate|i:1511276875;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ku66rbodtecc1fsap24lbvsgeml5m710','::1',1510738194,'__ci_last_regenerate|i:1510738194;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('mbpq55s04nn088l8rjdkt5ledb8u2dfp','::1',1511221921,'__ci_last_regenerate|i:1511221921;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('mfmmi6fdv956f0nc4mp9g32fb529987f','::1',1511180726,'__ci_last_regenerate|i:1511180726;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('mkge4mfghu9sl1rv5k5onb24a5its887','::1',1510737776,'__ci_last_regenerate|i:1510737776;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('nfsqu02803v44r0d8r1l172fk23dqkhl','::1',1510753342,'__ci_last_regenerate|i:1510753097;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ofm5cqo9mqjrpj2agb77t0c8h7cd8jfp','::1',1510838333,'__ci_last_regenerate|i:1510838333;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ojogjrnueje1lfa711eqdo62kajg7j5v','::1',1511051517,'__ci_last_regenerate|i:1511051517;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('pkp7bpmkq4ujbldsc013t7dm4leb47rt','::1',1510838340,'__ci_last_regenerate|i:1510838333;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('r3tcif20neojjurs5kp4api7hlqdumlo','::1',1510741281,'__ci_last_regenerate|i:1510741281;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('s9uijf6t8qio7mqlg7hitvcph4v2vnh4','::1',1511221030,'__ci_last_regenerate|i:1511221030;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('srvku0o4epit3p3m9n3paqe0df2n5jrn','::1',1511043859,'__ci_last_regenerate|i:1511043852;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('t1ljr35g5je2opje0shi5bqniedn9odo','::1',1511173761,'__ci_last_regenerate|i:1511173761;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('t6isuti9s7er7t901uark4vcb3o6oah3','::1',1511277179,'__ci_last_regenerate|i:1511277179;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('tc4hupp9grb2ilfuohhi1tqbbfgl9vft','::1',1511276332,'__ci_last_regenerate|i:1511276332;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('td2eljbpj625jjc6uv7ha3fcno475vft','::1',1511043852,'__ci_last_regenerate|i:1511043852;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('u3l696f30qk4h345ctdj8jv4lopkcskk','::1',1511084461,'__ci_last_regenerate|i:1511084461;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('v0qh1pb68sjpfe86itjhh345i01ftdpl','::1',1511217022,'__ci_last_regenerate|i:1511217022;'),('v3bvq7l10sd6ilbnqspn8s3vidqdvlgs','::1',1511282027,'__ci_last_regenerate|i:1511282012;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('vdgvhpobkc52ke6biptmouaki6rprq77','::1',1511268258,'__ci_last_regenerate|i:1511268258;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('veg2jgo0m28li43t708455gmpmfq9das','::1',1511181770,'__ci_last_regenerate|i:1511181770;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clase_optiuni`
--

DROP TABLE IF EXISTS `clase_optiuni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clase_optiuni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categorie` int(11) DEFAULT NULL,
  `nume` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clase_optiuni`
--

LOCK TABLES `clase_optiuni` WRITE;
/*!40000 ALTER TABLE `clase_optiuni` DISABLE KEYS */;
INSERT INTO `clase_optiuni` VALUES (1,1,'marime'),(2,3,'Cu gheata');
/*!40000 ALTER TABLE `clase_optiuni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comenzi`
--

DROP TABLE IF EXISTS `comenzi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comenzi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adr` text CHARACTER SET utf8 NOT NULL,
  `detalii` text CHARACTER SET utf8 NOT NULL,
  `plata` tinyint(1) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `confirmat` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comenzi`
--

LOCK TABLES `comenzi` WRITE;
/*!40000 ALTER TABLE `comenzi` DISABLE KEYS */;
INSERT INTO `comenzi` VALUES (1,'Sector 7','123',1,2,0),(3,'Calea Unirii 22','',1,2,1),(9,'Calea Unirii 22','',2,2,1),(8,'Calea Unirii 22','',2,2,0),(10,'Sector 7','',1,2,1);
/*!40000 ALTER TABLE `comenzi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingrediente`
--

DROP TABLE IF EXISTS `ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingrediente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` text COLLATE utf8mb4_unicode_ci,
  `pret_extra` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingrediente`
--

LOCK TABLES `ingrediente` WRITE;
/*!40000 ALTER TABLE `ingrediente` DISABLE KEYS */;
INSERT INTO `ingrediente` VALUES (1,'test',2.5),(2,'asdads',2.5);
/*!40000 ALTER TABLE `ingrediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingrediente_categorii`
--

DROP TABLE IF EXISTS `ingrediente_categorii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingrediente_categorii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ingredient` int(11) DEFAULT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingrediente_categorii`
--

LOCK TABLES `ingrediente_categorii` WRITE;
/*!40000 ALTER TABLE `ingrediente_categorii` DISABLE KEYS */;
INSERT INTO `ingrediente_categorii` VALUES (1,1,-1),(2,1,1);
/*!40000 ALTER TABLE `ingrediente_categorii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optiuni_produse`
--

DROP TABLE IF EXISTS `optiuni_produse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optiuni_produse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_produs` int(11) NOT NULL,
  `id_clasa_optiuni` int(11) DEFAULT NULL,
  `pret` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optiuni_produse`
--

LOCK TABLES `optiuni_produse` WRITE;
/*!40000 ALTER TABLE `optiuni_produse` DISABLE KEYS */;
INSERT INTO `optiuni_produse` VALUES (9,'medie (35 cm)',1,1,30),(8,'mica (22 cm)',1,1,25);
/*!40000 ALTER TABLE `optiuni_produse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optiuni_produse_comanda`
--

DROP TABLE IF EXISTS `optiuni_produse_comanda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optiuni_produse_comanda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_comanda` int(11) NOT NULL,
  `id_produs` int(11) NOT NULL,
  `id_optiune` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optiuni_produse_comanda`
--

LOCK TABLES `optiuni_produse_comanda` WRITE;
/*!40000 ALTER TABLE `optiuni_produse_comanda` DISABLE KEYS */;
INSERT INTO `optiuni_produse_comanda` VALUES (1,9,2,1),(2,9,2,3),(3,9,1,5),(5,10,1,5);
/*!40000 ALTER TABLE `optiuni_produse_comanda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produse`
--

DROP TABLE IF EXISTS `produse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categorie` int(11) DEFAULT NULL,
  `nume` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `descriere` text CHARACTER SET utf8,
  `image` varchar(255) DEFAULT NULL,
  `mica` varchar(255) DEFAULT NULL,
  `pret_mica` float DEFAULT NULL,
  `medie` varchar(255) DEFAULT NULL,
  `pret_medie` float DEFAULT NULL,
  `mare` varchar(255) DEFAULT NULL,
  `pret_mare` float DEFAULT NULL,
  `evidentiat` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produse`
--

LOCK TABLES `produse` WRITE;
/*!40000 ALTER TABLE `produse` DISABLE KEYS */;
INSERT INTO `produse` VALUES (1,1,'Pizza Capriciosa','pizza-capriciosa','<p>Pizza Capriciosa!!!</p>','13-1.jpg','20 cm',25.5,'30 cm',29.99,'40 cm',49.99,1),(2,1,'Pizza Carnivora','pizza-carnivora','<p>Pizza Carnivora</p>','2-1.jpg','15 cm',30,'25 cm',35,'30 cm',49.99,1),(6,1,'Summer Pizza','summer-pizza','<p>Shrimp, Red Capsicum, Green Capsicum, Onion, Chilli flakes, Lemon Pepper, Mozzarella, finished with Aioli</p>','10.jpg','15 cm',25.5,'25 cm',29.99,'40 cm',49.99,1),(7,1,'Pizza Vegetariana','pizza-vegetariana','<p>Pizza Vegetariana</p>','9.jpg','15 cm',25.5,'25 cm',29.99,'40 cm',0,0),(12,3,'cola','cola','<p>e buna</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(11,1,'da','da','<p>e buna</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `produse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produse_comanda`
--

DROP TABLE IF EXISTS `produse_comanda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produse_comanda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_comanda` int(11) NOT NULL,
  `id_produs` int(11) NOT NULL,
  `nume` varchar(255) NOT NULL,
  `marime` varchar(255) NOT NULL,
  `pret` float NOT NULL,
  `cantitate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produse_comanda`
--

LOCK TABLES `produse_comanda` WRITE;
/*!40000 ALTER TABLE `produse_comanda` DISABLE KEYS */;
INSERT INTO `produse_comanda` VALUES (1,1,2,'','15 cm',30,1),(11,10,1,'','30 cm',29.99,2),(3,3,1,'','20 cm',25.5,1),(4,4,2,'','25 cm',35,1),(5,5,6,'','25 cm',29.99,1),(8,8,2,'','25 cm',35,1),(9,9,2,'','25 cm',35,1),(10,9,1,'','20 cm',25.5,1);
/*!40000 ALTER TABLE `produse_comanda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produse_ingrediente`
--

DROP TABLE IF EXISTS `produse_ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produse_ingrediente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ingredient` int(11) DEFAULT NULL,
  `id_produs` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produse_ingrediente`
--

LOCK TABLES `produse_ingrediente` WRITE;
/*!40000 ALTER TABLE `produse_ingrediente` DISABLE KEYS */;
INSERT INTO `produse_ingrediente` VALUES (20,0,11),(22,0,11),(23,0,11),(26,0,11),(27,2,11),(28,0,12),(29,0,1),(31,0,1),(32,0,1),(33,1,1);
/*!40000 ALTER TABLE `produse_ingrediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useri`
--

DROP TABLE IF EXISTS `useri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nume` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_pass` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tip` int(11) NOT NULL COMMENT '1 - amin / 2 - client',
  `telefon` varchar(255) NOT NULL,
  `oras` varchar(255) CHARACTER SET utf8 NOT NULL,
  `adr` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useri`
--

LOCK TABLES `useri` WRITE;
/*!40000 ALTER TABLE `useri` DISABLE KEYS */;
INSERT INTO `useri` VALUES (1,'contact@yummpizza.ro','Victor','4297f44b13955235245b2497399d7a93',1,'','Suceava','Calea Unirii 22'),(2,'victor.tudosa@gmail.com','test','4297f44b13955235245b2497399d7a93',2,'0746831133','Bucuresti','Sector 7');
/*!40000 ALTER TABLE `useri` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-21 18:36:11
