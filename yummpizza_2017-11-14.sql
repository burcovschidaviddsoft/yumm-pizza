# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.2.7-MariaDB)
# Database: yummpizza
# Generation Time: 2017-11-14 18:39:20 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categorii
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categorii`;

CREATE TABLE `categorii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `categorii` WRITE;
/*!40000 ALTER TABLE `categorii` DISABLE KEYS */;

INSERT INTO `categorii` (`id`, `nume`, `slug`)
VALUES
	(1,'Pizza','pizza'),
	(2,'test teste','test-teste'),
	(3,'Sucuri','sucuri');

/*!40000 ALTER TABLE `categorii` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`)
VALUES
	('9upjtntgnmqcqhvirnk8ea3gfq5tu77d','127.0.0.1',1510684025,X'5F5F63695F6C6173745F726567656E65726174657C693A313531303638343031373B636F737C613A313A7B693A303B613A363A7B733A393A2269645F70726F647573223B733A313A2232223B733A343A226E756D65223B733A31353A2250697A7A61204361726E69766F7261223B733A363A226D6172696D65223B733A353A22313520636D223B733A343A2270726574223B733A323A223330223B733A393A2263616E746974617465223B693A313B733A373A226F707469756E69223B613A313A7B693A303B733A313A2231223B7D7D7D'),
	('bb65c1h739v74vb4ijncmidkavats02b','127.0.0.1',1510684017,X'5F5F63695F6C6173745F726567656E65726174657C693A313531303638343031373B');

/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comenzi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comenzi`;

CREATE TABLE `comenzi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adr` text CHARACTER SET utf8 NOT NULL,
  `detalii` text CHARACTER SET utf8 NOT NULL,
  `plata` tinyint(1) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `confirmat` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `comenzi` WRITE;
/*!40000 ALTER TABLE `comenzi` DISABLE KEYS */;

INSERT INTO `comenzi` (`id`, `adr`, `detalii`, `plata`, `id_user`, `confirmat`)
VALUES
	(1,'Sector 7','123',1,2,0),
	(3,'Calea Unirii 22','',1,2,1),
	(9,'Calea Unirii 22','',2,2,1),
	(8,'Calea Unirii 22','',2,2,0),
	(10,'Sector 7','',1,2,1);

/*!40000 ALTER TABLE `comenzi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table optiuni_produse
# ------------------------------------------------------------

DROP TABLE IF EXISTS `optiuni_produse`;

CREATE TABLE `optiuni_produse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_produs` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `optiuni_produse` WRITE;
/*!40000 ALTER TABLE `optiuni_produse` DISABLE KEYS */;

INSERT INTO `optiuni_produse` (`id`, `nume`, `id_produs`)
VALUES
	(1,'Extra ciuperci',2),
	(5,'Extra branza',1),
	(3,'Fara rosii',2);

/*!40000 ALTER TABLE `optiuni_produse` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table optiuni_produse_comanda
# ------------------------------------------------------------

DROP TABLE IF EXISTS `optiuni_produse_comanda`;

CREATE TABLE `optiuni_produse_comanda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_comanda` int(11) NOT NULL,
  `id_produs` int(11) NOT NULL,
  `id_optiune` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `optiuni_produse_comanda` WRITE;
/*!40000 ALTER TABLE `optiuni_produse_comanda` DISABLE KEYS */;

INSERT INTO `optiuni_produse_comanda` (`id`, `id_comanda`, `id_produs`, `id_optiune`)
VALUES
	(1,9,2,1),
	(2,9,2,3),
	(3,9,1,5),
	(5,10,1,5);

/*!40000 ALTER TABLE `optiuni_produse_comanda` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table produse
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produse`;

CREATE TABLE `produse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categorie` int(11) DEFAULT NULL,
  `nume` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `descriere` text CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `mica` varchar(255) DEFAULT NULL,
  `pret_mica` float DEFAULT NULL,
  `medie` varchar(255) DEFAULT NULL,
  `pret_medie` float DEFAULT NULL,
  `mare` varchar(255) DEFAULT NULL,
  `pret_mare` float DEFAULT NULL,
  `evidentiat` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `produse` WRITE;
/*!40000 ALTER TABLE `produse` DISABLE KEYS */;

INSERT INTO `produse` (`id`, `id_categorie`, `nume`, `slug`, `descriere`, `image`, `mica`, `pret_mica`, `medie`, `pret_medie`, `mare`, `pret_mare`, `evidentiat`)
VALUES
	(1,1,'Pizza Capriciosa','pizza-capriciosa','<p>Pizza Capriciosa!!!</p>','13-1.jpg','20 cm',25.5,'30 cm',29.99,'40 cm',49.99,1),
	(2,1,'Pizza Carnivora','pizza-carnivora','<p>Pizza Carnivora</p>','2-1.jpg','15 cm',30,'25 cm',35,'30 cm',49.99,1),
	(6,1,'Summer Pizza','summer-pizza','<p>Shrimp, Red Capsicum, Green Capsicum, Onion, Chilli flakes, Lemon Pepper, Mozzarella, finished with Aioli</p>','10.jpg','15 cm',25.5,'25 cm',29.99,'40 cm',49.99,1),
	(7,1,'Pizza Vegetariana','pizza-vegetariana','<p>Pizza Vegetariana</p>','9.jpg','15 cm',25.5,'25 cm',29.99,'40 cm',0,0);

/*!40000 ALTER TABLE `produse` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table produse_comanda
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produse_comanda`;

CREATE TABLE `produse_comanda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_comanda` int(11) NOT NULL,
  `id_produs` int(11) NOT NULL,
  `nume` varchar(255) NOT NULL,
  `marime` varchar(255) NOT NULL,
  `pret` float NOT NULL,
  `cantitate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `produse_comanda` WRITE;
/*!40000 ALTER TABLE `produse_comanda` DISABLE KEYS */;

INSERT INTO `produse_comanda` (`id`, `id_comanda`, `id_produs`, `nume`, `marime`, `pret`, `cantitate`)
VALUES
	(1,1,2,'','15 cm',30,1),
	(11,10,1,'','30 cm',29.99,2),
	(3,3,1,'','20 cm',25.5,1),
	(4,4,2,'','25 cm',35,1),
	(5,5,6,'','25 cm',29.99,1),
	(8,8,2,'','25 cm',35,1),
	(9,9,2,'','25 cm',35,1),
	(10,9,1,'','20 cm',25.5,1);

/*!40000 ALTER TABLE `produse_comanda` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table useri
# ------------------------------------------------------------

DROP TABLE IF EXISTS `useri`;

CREATE TABLE `useri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nume` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_pass` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tip` int(11) NOT NULL COMMENT '1 - amin / 2 - client',
  `telefon` varchar(255) NOT NULL,
  `oras` varchar(255) CHARACTER SET utf8 NOT NULL,
  `adr` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `useri` WRITE;
/*!40000 ALTER TABLE `useri` DISABLE KEYS */;

INSERT INTO `useri` (`id`, `user_email`, `nume`, `user_pass`, `tip`, `telefon`, `oras`, `adr`)
VALUES
	(1,'contact@yummpizza.ro','Victor','4297f44b13955235245b2497399d7a93',1,'','Suceava','Calea Unirii 22'),
	(2,'victor.tudosa@gmail.com','test','4297f44b13955235245b2497399d7a93',2,'0746831133','Bucuresti','Sector 7');

/*!40000 ALTER TABLE `useri` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
