-- MySQL dump 10.16  Distrib 10.1.28-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: yummpizza
-- ------------------------------------------------------
-- Server version	10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorii`
--

DROP TABLE IF EXISTS `categorii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorii` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorii`
--

LOCK TABLES `categorii` WRITE;
/*!40000 ALTER TABLE `categorii` DISABLE KEYS */;
INSERT INTO `categorii` VALUES (1,'Pizza','pizza'),(2,'test teste','test-teste'),(3,'Sucuri','sucuri');
/*!40000 ALTER TABLE `categorii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('3cee8bgd7vsf97c9esalrg936i05cm1b','::1',1510836119,'__ci_last_regenerate|i:1510836119;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3e0eg1aseprd220tra97jd3gof3hgq69','::1',1510753097,'__ci_last_regenerate|i:1510753097;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3fagacr06mbb1bquejdh7hhrkd5pejhg','::1',1510749680,'__ci_last_regenerate|i:1510749680;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('3i572p4djbt9ai7lhpmq8gtrdon87jnt','::1',1510736993,'__ci_last_regenerate|i:1510736993;'),('3qfc14mlq9mmfi0f1j3o7978akekgc5f','::1',1510737365,'__ci_last_regenerate|i:1510737365;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('4hgm6tqcdi02liu24pgrr95v1sm8pg0n','::1',1510752795,'__ci_last_regenerate|i:1510752795;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('7hlf862pglk53d6bc57kfpq183hue5u7','::1',1510751772,'__ci_last_regenerate|i:1510751772;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('9r8gqp3kbjc8prlhnfoegdtlg6gdni75','::1',1510752492,'__ci_last_regenerate|i:1510752492;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('boqmk6baioa2h12aos8qic9ck7c62kvj','::1',1510741322,'__ci_last_regenerate|i:1510741281;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('c8bi5hc1c7d648iqf5kkla48445ptpvq','::1',1510740480,'__ci_last_regenerate|i:1510740480;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('dc47ubqjna7n3tt8499frb8ig2dpva5s','::1',1510751231,'__ci_last_regenerate|i:1510751231;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ini49om2qlcbvalmbbccg6hnod7ik543','::1',1510750800,'__ci_last_regenerate|i:1510750800;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ku66rbodtecc1fsap24lbvsgeml5m710','::1',1510738194,'__ci_last_regenerate|i:1510738194;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('mkge4mfghu9sl1rv5k5onb24a5its887','::1',1510737776,'__ci_last_regenerate|i:1510737776;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('nfsqu02803v44r0d8r1l172fk23dqkhl','::1',1510753342,'__ci_last_regenerate|i:1510753097;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('ofm5cqo9mqjrpj2agb77t0c8h7cd8jfp','::1',1510838333,'__ci_last_regenerate|i:1510838333;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('pkp7bpmkq4ujbldsc013t7dm4leb47rt','::1',1510838340,'__ci_last_regenerate|i:1510838333;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}'),('r3tcif20neojjurs5kp4api7hlqdumlo','::1',1510741281,'__ci_last_regenerate|i:1510741281;login|a:9:{s:2:\"id\";s:1:\"1\";s:10:\"user_email\";s:20:\"contact@yummpizza.ro\";s:4:\"nume\";s:6:\"Victor\";s:3:\"tip\";s:1:\"1\";s:7:\"telefon\";s:0:\"\";s:4:\"oras\";s:7:\"Suceava\";s:3:\"adr\";s:15:\"Calea Unirii 22\";s:4:\"user\";s:20:\"contact@yummpizza.ro\";s:9:\"logged_in\";b:1;}');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clase_optiuni`
--

DROP TABLE IF EXISTS `clase_optiuni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clase_optiuni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categorie` int(11) DEFAULT NULL,
  `nume` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clase_optiuni`
--

LOCK TABLES `clase_optiuni` WRITE;
/*!40000 ALTER TABLE `clase_optiuni` DISABLE KEYS */;
/*!40000 ALTER TABLE `clase_optiuni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comenzi`
--

DROP TABLE IF EXISTS `comenzi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comenzi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adr` text CHARACTER SET utf8 NOT NULL,
  `detalii` text CHARACTER SET utf8 NOT NULL,
  `plata` tinyint(1) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `confirmat` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comenzi`
--

LOCK TABLES `comenzi` WRITE;
/*!40000 ALTER TABLE `comenzi` DISABLE KEYS */;
INSERT INTO `comenzi` VALUES (1,'Sector 7','123',1,2,0),(3,'Calea Unirii 22','',1,2,1),(9,'Calea Unirii 22','',2,2,1),(8,'Calea Unirii 22','',2,2,0),(10,'Sector 7','',1,2,1);
/*!40000 ALTER TABLE `comenzi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingrediente`
--

DROP TABLE IF EXISTS `ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingrediente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` text COLLATE utf8mb4_unicode_ci,
  `pret_extra` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingrediente`
--

LOCK TABLES `ingrediente` WRITE;
/*!40000 ALTER TABLE `ingrediente` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingrediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optiuni_produse`
--

DROP TABLE IF EXISTS `optiuni_produse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optiuni_produse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_produs` int(11) NOT NULL,
  `id_clasa_optiuni` int(11) DEFAULT NULL,
  `pret` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optiuni_produse`
--

LOCK TABLES `optiuni_produse` WRITE;
/*!40000 ALTER TABLE `optiuni_produse` DISABLE KEYS */;
INSERT INTO `optiuni_produse` VALUES (1,'Extra ciuperci',2,NULL,NULL),(5,'Extra branza',1,NULL,NULL),(3,'Fara rosii',2,NULL,NULL);
/*!40000 ALTER TABLE `optiuni_produse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optiuni_produse_comanda`
--

DROP TABLE IF EXISTS `optiuni_produse_comanda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optiuni_produse_comanda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_comanda` int(11) NOT NULL,
  `id_produs` int(11) NOT NULL,
  `id_optiune` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optiuni_produse_comanda`
--

LOCK TABLES `optiuni_produse_comanda` WRITE;
/*!40000 ALTER TABLE `optiuni_produse_comanda` DISABLE KEYS */;
INSERT INTO `optiuni_produse_comanda` VALUES (1,9,2,1),(2,9,2,3),(3,9,1,5),(5,10,1,5);
/*!40000 ALTER TABLE `optiuni_produse_comanda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produse`
--

DROP TABLE IF EXISTS `produse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categorie` int(11) DEFAULT NULL,
  `nume` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `descriere` text CHARACTER SET utf8,
  `image` varchar(255) DEFAULT NULL,
  `mica` varchar(255) DEFAULT NULL,
  `pret_mica` float DEFAULT NULL,
  `medie` varchar(255) DEFAULT NULL,
  `pret_medie` float DEFAULT NULL,
  `mare` varchar(255) DEFAULT NULL,
  `pret_mare` float DEFAULT NULL,
  `evidentiat` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produse`
--

LOCK TABLES `produse` WRITE;
/*!40000 ALTER TABLE `produse` DISABLE KEYS */;
INSERT INTO `produse` VALUES (1,1,'Pizza Capriciosa','pizza-capriciosa','<p>Pizza Capriciosa!!!</p>','13-1.jpg','20 cm',25.5,'30 cm',29.99,'40 cm',49.99,1),(2,1,'Pizza Carnivora','pizza-carnivora','<p>Pizza Carnivora</p>','2-1.jpg','15 cm',30,'25 cm',35,'30 cm',49.99,1),(6,1,'Summer Pizza','summer-pizza','<p>Shrimp, Red Capsicum, Green Capsicum, Onion, Chilli flakes, Lemon Pepper, Mozzarella, finished with Aioli</p>','10.jpg','15 cm',25.5,'25 cm',29.99,'40 cm',49.99,1),(7,1,'Pizza Vegetariana','pizza-vegetariana','<p>Pizza Vegetariana</p>','9.jpg','15 cm',25.5,'25 cm',29.99,'40 cm',0,0);
/*!40000 ALTER TABLE `produse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produse_comanda`
--

DROP TABLE IF EXISTS `produse_comanda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produse_comanda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_comanda` int(11) NOT NULL,
  `id_produs` int(11) NOT NULL,
  `nume` varchar(255) NOT NULL,
  `marime` varchar(255) NOT NULL,
  `pret` float NOT NULL,
  `cantitate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produse_comanda`
--

LOCK TABLES `produse_comanda` WRITE;
/*!40000 ALTER TABLE `produse_comanda` DISABLE KEYS */;
INSERT INTO `produse_comanda` VALUES (1,1,2,'','15 cm',30,1),(11,10,1,'','30 cm',29.99,2),(3,3,1,'','20 cm',25.5,1),(4,4,2,'','25 cm',35,1),(5,5,6,'','25 cm',29.99,1),(8,8,2,'','25 cm',35,1),(9,9,2,'','25 cm',35,1),(10,9,1,'','20 cm',25.5,1);
/*!40000 ALTER TABLE `produse_comanda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produse_ingrediente`
--

DROP TABLE IF EXISTS `produse_ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produse_ingrediente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ingredient` int(11) DEFAULT NULL,
  `id_produs` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produse_ingrediente`
--

LOCK TABLES `produse_ingrediente` WRITE;
/*!40000 ALTER TABLE `produse_ingrediente` DISABLE KEYS */;
/*!40000 ALTER TABLE `produse_ingrediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useri`
--

DROP TABLE IF EXISTS `useri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nume` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_pass` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tip` int(11) NOT NULL COMMENT '1 - amin / 2 - client',
  `telefon` varchar(255) NOT NULL,
  `oras` varchar(255) CHARACTER SET utf8 NOT NULL,
  `adr` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useri`
--

LOCK TABLES `useri` WRITE;
/*!40000 ALTER TABLE `useri` DISABLE KEYS */;
INSERT INTO `useri` VALUES (1,'contact@yummpizza.ro','Victor','4297f44b13955235245b2497399d7a93',1,'','Suceava','Calea Unirii 22'),(2,'victor.tudosa@gmail.com','test','4297f44b13955235245b2497399d7a93',2,'0746831133','Bucuresti','Sector 7');
/*!40000 ALTER TABLE `useri` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-18 12:33:55
